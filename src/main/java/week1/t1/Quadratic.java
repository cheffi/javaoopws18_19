package week1.t1;

import java.util.Scanner;


public class Quadratic {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        double b = scan.nextDouble();
        double c = scan.nextDouble();

        double discriminant = b * b - 4.0 * c;

        if (discriminant > 0) {

            double sqroot = Math.sqrt(discriminant);

            double root1 = (- b + sqroot) / 2.0;
            double root2 = (- b - sqroot) / 2.0;

            System.out.println(root1);
            System.out.println(root2);
        } else if (discriminant == 0) {
            System.out.println("The discriminant is equal to zero.");
        } else {
            System.out.println("The discriminant is smaller than zero.");
        }
    }
}








