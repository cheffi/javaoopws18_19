package week1.t4;

import java.util.Random;


public class RandomSeqLotto {

    public static void main(String[] args) {

        int a = 1;
        int b = 49;
        int n = 6;

        Random rand = new Random();

        // generate and print n numbers between 0 and 1
        for (int i = 0; i < n; i++) {

            System.out.print(rand.nextInt(b - a) + a);
            System.out.print(" ");

        }
    }
}
