package week1.t3;

import java.util.Random;
import java.util.Scanner;


public class RandomSeqAB {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        // command-line argument
        try {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            int n = scanner.nextInt();
            Random rand = new Random();

            // generate and print n numbers between 0 and 1
            for (int i = 0; i < n; i++) {
                int rndm = rand.nextInt(b - a) + a;

                System.out.println(rndm);
            }
        } catch (IllegalArgumentException e) {
            System.out.print(e.getMessage());
        }


    }


}

