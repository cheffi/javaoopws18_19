package week1.t6;

import java.util.Scanner;

public class LastFactorialDigit {
    public static void main(String[] args) {


        int counter, anzEingaben;
        Scanner read = new Scanner(System.in);
            anzEingaben = read.nextInt();
        for (int i = 0; i <anzEingaben; i++) {

            counter = read.nextInt();
            int fak = 1;
            for (int j = 1; j <= counter; ++ j) {
                fak *= j;
            }
            fak = fak % 10;
            System.out.println(fak);
        }

    }
}
