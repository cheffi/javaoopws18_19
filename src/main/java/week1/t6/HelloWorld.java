package week1.t6;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Hello World!
 * Link:https://open.kattis.com/problems/
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime:0.05s
 */

public class HelloWorld {

    public static void main (String[] args) { 
        
        System.out.println("Hello World!"); 
        
    } 
    
    
}