package week1.t6;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem:Identifying Map Tiles
 * Link:https://open.kattis.com/problems/maptiles2
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 22/10/2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime:0.07s
 */


public class maptiles2 {

    public static void main(String[] args) {
        // write your code here
        Scanner scanner = new Scanner(System.in);
        String buffer;
        int x = 0;
        int y = 0;
        int level;
        buffer = scanner.nextLine();
        level = buffer.length();

        int MAX = 31;
        for (int i = 0; i < level; i++) {
            if (buffer.charAt(i) == '1' || buffer.charAt(i) == '3') {
                x += (1 << level) >> (i + 1);
            }//if
            if (buffer.charAt(i) == '2' || buffer.charAt(i) == '3') {
                y += (1 << level) >> (i + 1);

            }

        }
        System.out.printf("%d %d %d", level, x, y);

    }

}