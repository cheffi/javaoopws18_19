package week1.t6;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Stuck in a Timeloop
 * Link:https://open.kattis.com/problems/timeloop
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime:0.08s
 */

public class StuckInTimeloop {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);

           int zahl = sc.nextInt();
           int count;
           for(count = 1;count<=zahl;count++)
           {
        
            
             System.out.println(count + " Abracadabra");
            }
            
            
        }
}