package week1.t6;

import java.util.Scanner;

public class Flip {

    public static void main(String[] args) {

        Scanner reader = new Scanner(System.in);

        int secondDigit = Integer.parseInt(new StringBuilder(reader.next()).reverse().toString());
        int firstDigit = Integer.parseInt(new StringBuilder(reader.next()).reverse().toString());

        if (firstDigit >= secondDigit){
            System.out.println(firstDigit);
        }else
            System.out.println(secondDigit);


    }
}
