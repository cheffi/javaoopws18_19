package week1.t6;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Just in a Minute
 * Link:https://open.kattis.com/problems/justaminute
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime:0.13s
 */

public class justaminute{


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int anzBeobachtungen = Integer.parseInt(scanner.nextLine());
        double minuten =0;
        double sekunden =0;
        double durchschnitt=0;
        for(int i = 0; i<anzBeobachtungen; i++){
            String[] observation = scanner.nextLine().split(" ");
            minuten += Double.parseDouble(observation[0]);
            sekunden += Double.parseDouble(observation[1]);
            durchschnitt = sekunden /(minuten*60);

        }///for

        if (durchschnitt<=1) {
            System.out.println("acm.acm.measurement error");
        } else {
            System.out.format("%.9f", durchschnitt);
        }
    }//main


}