package week1.t6;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Jumbled Compass
 * Link:https://open.kattis.com/problems/compass
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime:0.07s
 */


public class compass {


    public static void main(String[] args){
        int x;
        int y;
        int range;

        Scanner scan = new Scanner(System.in);
        x = scan.nextInt();
        y = scan.nextInt();
        range = y-x;
        if(Math.abs(range) == 180 ){
            System.out.print(180);
        } else if (Math.abs(range)<180){
            System.out.print(range);
        } else if (range < 0){
            System.out.print(360+range);
        } else {
            System.out.print(range-360);
        }
    }


}
