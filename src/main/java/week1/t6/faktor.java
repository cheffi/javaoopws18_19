package week1.t6;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Faktor
 * Link:https://open.kattis.com/problems/faktor
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime:0.07s
 */


public class faktor {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt(), i = sc.nextInt();
        int x = a * i;
        while (((double) --x / a) > i - 1) {
        }
        System.out.println(++x);
    }
}