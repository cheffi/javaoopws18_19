package week1.t6;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Jabuke
 * Link: https://open.kattis.com/problems/jabuke
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 23/10/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.09 s
 */
import java.util.Scanner;

public class Jabuke {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int x1 = input.nextInt();
		int y1 = input.nextInt();
		int x2 = input.nextInt();
		int y2 = input.nextInt();
		int x3 = input.nextInt();
		int y3 = input.nextInt();
		int appleTrees = input.nextInt();
		int antesTrees = 0;
		double antesArea = calculateArea(x1, y1, x2, y2, x3, y3);

		for (int i = 0; i < appleTrees; i++) {
			int x = input.nextInt();
			int y = input.nextInt();
			if (withinArea(antesArea, x1, y1, x2, y2, x3, y3, x, y)) {
				antesTrees++;
			}

		}
		System.out.printf("%.1f ", antesArea);
		System.out.println(antesTrees);

	}

	public static double calculateArea(int x1, int y1, int x2, int y2, int x3, int y3) {
		return Math.abs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2.0;
	}

	public static boolean withinArea(double antesArea, int x1, int y1, int x2, int y2, int x3, int y3, int x, int y) {
		double areaA = calculateArea(x, y, x2, y2, x3, y3);
		double areaB = calculateArea(x1, y1, x, y, x3, y3);
		double areaC = calculateArea(x1, y1, x2, y2, x, y);

		return (antesArea == (areaA + areaB + areaC));
	}

}
