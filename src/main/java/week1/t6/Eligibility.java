package week1.t6;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Eligibility
 * Link: https://open.kattis.com/contests/eu6hf6/problems/eligibility;
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 10/22/2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime: 0.17s
 */
import java.util.Scanner;

public class Eligibility {

    public static void main(String[] args) {

        //new scanner, count value, 2d Array
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        String field[][] = new String[count][4];


        //fill in seperated values from string input line
        for(int i = 0; i < count; i++) {
            field[i][0] = scanner.next();
            field[i][1] = scanner.next().substring(0, 4);
            field[i][2] = scanner.next().substring(0, 4);
            field[i][3] = scanner.next();
        }

        //conditions:
        //1: post date => 2010  eligible
        //2: born date=> 1991  eligible
        //3: courses >= 41  ineligible
        //4: else  coach petitions


        //proof if the conditons are true or false
        //then output the result
        for(int i = 0; i < count; i++) {
            if(Integer.valueOf(field[i][1]) >= 2010) {
                System.out.println(field[i][0] + " " + "eligible");
            }else if(Integer.valueOf(field[i][2]) >= 1991) {
                System.out.println(field[i][0] + " " + "eligible");
            }else if(Integer.valueOf(field[i][3]) >= 41) {
                System.out.println(field[i][0] + " " + "ineligible");
            }else {
                System.out.println(field[i][0] + " " + "coach petitions");
            }
        }
    }
}
