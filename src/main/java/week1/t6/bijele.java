package week1.t6;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Bijele
 * Link:https://open.kattis.com/problems/bijele
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime:0.07s
 */


public class bijele {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int[] arr = {1 , 1 , 2 , 2 , 2 , 8};
        int nums;
        int i=0;
        while(i<6){
            nums = scan.nextInt();
            System.out.print(-nums+arr[i]+" ");
            ++i;
        }
    }
}