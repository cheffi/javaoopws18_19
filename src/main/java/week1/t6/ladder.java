package week1.t6;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Ladder
 * Link:https://open.kattis.com/problems/ladder
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime:0.08s
 */


public class ladder
{


    public static void main(String[] args) {
        // write your code here


        Scanner scanner = new Scanner(System.in);
        double h;
        double v;
        String input = scanner.nextLine();
        Scanner scanner2 = new Scanner(input);
        h = scanner2.nextDouble();
        v = scanner2.nextDouble();


        System.out.println((int)(Math.ceil(h/(Math.sin(v*Math.PI/180)))));

    }



}
