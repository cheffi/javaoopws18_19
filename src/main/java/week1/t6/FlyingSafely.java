package week1.t6;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Flying safely
 * Link:https://open.kattis.com/problems/flyingsafely
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 21/10/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.65 s
 */
import java.util.Scanner;


public class FlyingSafely {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		int test_case;
		test_case = input.nextInt();
		
		while(test_case>=0) {
			int a, b, c = 0;
			a = input.nextInt();
			b = input.nextInt();
			System.out.printf("%d \n", a-1);
			
			while(b>=0) {
				a = input.nextInt();
				c= input.nextInt();
				b--;
			}
			test_case--;
		}
	}

}
