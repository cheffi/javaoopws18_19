package week1.t6;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Cudoviste
 * Link: https://open.kattis.com/contests/eu6hf6/problems/cudoviste;
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 10/23/2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime: 0.13s
 */
import java.util.Scanner;

public class Cudoviste {

    public static void main(String[] args) {


        //read input with scanner
        Scanner scan = new Scanner(System.in);

        //first int for rows
        //second int for cols
        //all squashes equal 0
        int rows = scan.nextInt();
        int columns = scan.nextInt();
        int squashZero = 0;
        int squashOne = 0;
        int squashTwo = 0;
        int squashThree = 0;
        int squashFour = 0;
        String array[][] = new String[rows][columns];
        Boolean b;

        //for loop to set all characters in an array[i][j]
        for(int i = 0; i < rows; i++){
            String next = scan.next();
            for(int j = 0; j < columns; j++){
                array[i][j] = next.substring(j, j+1);
            }
        }


        //for loop to see where a truck can park
        for(int i = 0; i < rows - 1; i++){
            for(int j = 0; j < columns - 1; j++){

                //check all indexes equal #
                if(array[i][j].matches("#")) {
                    b = false;
                }
                else if(array[i][j+1].matches("#")) {
                    b = false;
                }
                else if(array[i+1][j].matches("#")) {
                    b = false;
                }
                else b = !array[i + 1][j + 1].matches("#");

                //if true -> continue with counting
                //else -> next iteration
                if(b) {

                    //count the X`s where a car is parking
                    int count = 0;
                    if(array[i][j].matches("X")) {
                        count++;
                    }
                    if(array[i][j+1].matches("X")) {
                        count++;
                    }
                    if(array[i+1][j].matches("X")) {
                        count++;
                    }
                    if(array[i+1][j+1].matches("X")) {
                        count++;
                    }

                    //check how many cars can be squashed
                    switch(count) {
                        case 0: squashZero++;
                            break;
                        case 1: squashOne++;
                            break;
                        case 2: squashTwo++;
                            break;
                        case 3: squashThree++;
                            break;
                        case 4: squashFour++;
                            break;
                        default: break;
                    }
                }
            }
        }
        //print out all results for squashing
        System.out.println(squashZero);
        System.out.println(squashOne);
        System.out.println(squashTwo);
        System.out.println(squashThree);
        System.out.println(squashFour);
    }
}
