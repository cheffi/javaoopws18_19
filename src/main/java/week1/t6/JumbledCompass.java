package week1.t6;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Jumbled compass
 * Link:https://open.kattis.com/problems/compass
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 21/10/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.07 s
 */
import java.util.Scanner;
public class JumbledCompass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int a;
		int b ;
		a = input.nextInt();
		b= input.nextInt();
		if(a==b) {
			System.out.print("0\n");
		}
		else if(a<b) {
			int dif1 = b-a;
			int dif2 = (360-b)+a;
			if(dif1<=dif2) {
				System.out.print(dif1);
			}else {
				System.out.print(-dif2);
			}
			
		}else {
			int dif1 = a-b;
			int dif2 = (360-a)+b;
			if(dif1<dif2) {
				System.out.print(-dif1);
			}else {
				System.out.print(dif2);
			}
			
		}
	}

}
