package week1.t6;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Identifying Map Tiles
 * Link: https://open.kattis.com/problems/maptiles2
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 22/10/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.07 s
 */
import java.util.Scanner;

public class MapTiles {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String quadkey = sc.nextLine();

        int zoom = quadkey.length();
        int x = 0;
        int y = 0;

        for (int i = 0; i < quadkey.length(); i++) {

            char c = quadkey.charAt(i);
            int offset = (int) Math.pow(2, quadkey.length() - i - 1);

            if(c == '1' || c == '3'){
                x += offset;
            }
            if(c == '2' || c == '3'){
                y += offset;
            }
        }
        System.out.println(zoom + " " + x + " " + y);

        sc.close();
    }
}
