package week1.t6;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Different Distances
 * Link: https://open.kattis.com/contests/eu6hf6/problems/differentdistances;
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 10/24/2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime: 0.23s
 */
import java.util.ArrayList;
import java.util.Scanner;

public class Distances {

    public static void main(String[] args) {


        //set scanner, list and counting value
        Double result;
        int count = 0;
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> list = new ArrayList<String>();


        //get input values while != 0
        String line;
        while((line = scanner.next()) != null) {
            if((line.equals("0"))) {
                break;
            }
            list.add(line);
            count++;
        }

        //set array with all list values
        //five values for each
        String[][] array = new String[count][5];
        int k = 0;

        for(int i = 0; i < count / 5; i++) {
            for(int j = 0; j < 5; j++) {
                array[i][j] = list.get(k);
                k++;
            }
        }


        Double x1[] = new Double[count];
        Double y1[] = new Double[count];
        Double x2[] = new Double[count];
        Double y2[] = new Double[count];
        Double p[] = new Double[count];


        //set double values for variables in p-norm
        for(int i = 0; i < (count / 5); i++) {
            x1[i] = Double.parseDouble(array[i][0]);
            y1[i] = Double.parseDouble(array[i][1]);
            x2[i] = Double.parseDouble(array[i][2]);
            y2[i] = Double.parseDouble(array[i][3]);
            p[i] =  Double.parseDouble(array[i][4]);

        }


        StringBuilder s;
        //print all results for the calculation of the p-norm
        for(int i = 0; i < count / 5; i++) {

            //first set result as Double value without rounding
            result = Math.pow((Math.pow((Math.abs(x1[i] - x2[i])), p[i]) + Math.pow((Math.abs(y1[i] - y2[i])), p[i])), 1.0/p[i]);

            //then round to ten characters after the point
            result = Math.round(10000000000.0 * result) / 10000000000.0;

            //split into numbers after point
            s = new StringBuilder(result.toString());
            String[] splitter = result.toString().split("\\.");
            int b = splitter[1].length();

            //if smaller than ten numbers after the point add "0" till 10
            if(b < 10) {
                for(int f = 0; f < 10 - b; f++) {
                    s.append("0");
                }
            }

            //print all results
            System.out.println(s);
        }
    }
}