package week1.t6;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Yoda.java
 * Link: https://open.kattis.com/problems/yoda
 * Link:https://open.kattis.com/problems/yoda*
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime:0.07s
 */

import java.util.Scanner;

public class Yoda {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        StringBuilder n1 = new StringBuilder(sc.nextLine());
        StringBuilder n2 = new StringBuilder(sc.nextLine());

        StringBuilder o1 = new StringBuilder();
        StringBuilder o2 = new StringBuilder();

        while (n1.length() != n2.length())
        {
            if (n1.length() < n2.length())
                n1.insert(0, "0");
            else
                n2.insert(0, "0");
        }

        for (int i = n1.length() - 1; i >= 0; i--) {
            char one = n1.charAt(i);
            char two = n2.charAt(i);

            if(one == two){
                o1.insert(0, one);
                o2.insert(0, two);
            } else if(one < two){
                o2.insert(0, two);
            } else {
                o1.insert(0, one);
            }
        }

        if (o1.toString().equals(""))
            System.out.println("YODA");
        else
            System.out.println(Integer.parseInt(o1.toString()));

        if (o2.toString().equals(""))
            System.out.println("YODA");
        else
            System.out.println(Integer.parseInt(o2.toString()));

        sc.close();
    }
}