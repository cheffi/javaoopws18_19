package week1.t6;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Quadrant Selection
 * Link:https://open.kattis.com/problems/quadrants
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime:0.08s
 */

public class Quadrants {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double x = scan.nextDouble();
        double y = scan.nextDouble();
        int quad = quadrant(x,y);
        System.out.println(quad);
    }

    public static int quadrant(double x, double y) {
        if (x > 0 && y > 0) {
            return 1;
        } else if (x < 0 && y > 0) {
            return 2;
        } else if (x < 0 && y < 0) {
            return 3;
        } else if (x > 0 && y < 0) {
            return 4;
        } else {
            return 0;
        }
    }
}