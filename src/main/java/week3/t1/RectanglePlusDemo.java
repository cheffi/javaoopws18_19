package week3.t1;

public class RectanglePlusDemo {

    public static void main(String[] args) {


        Point point = new Point(5, 10);


        RectanglePlus rectangle = new RectanglePlus();
        rectangle.origin = point;
        System.out.println(rectangle.toString());

        RectanglePlus rectangleWithPoint = new RectanglePlus(point);
        rectangleWithPoint.move(20, 20);
        rectangleWithPoint.height = 10;
        rectangleWithPoint.width = 6;
        System.out.println(rectangleWithPoint.toString());

        RectanglePlus rectangleWithCoordinates = new RectanglePlus(1, 2);
        System.out.println(rectangleWithCoordinates.toString());

        RectanglePlus rectangleWithPointAndCoordinates = new RectanglePlus(point, 3, 3);
        System.out.println(rectangleWithPointAndCoordinates.toString());

        rectangleWithPoint.move(5, 5);
        System.out.println("New rectangle with pointobject \n" + rectangleWithPoint.toString());

        System.out.println("Compare two rectangle: \n" + rectangle.isLargerThan(rectangleWithCoordinates));
        System.out.println(rectangle.getClass());
        System.out.println(rectangle.hashCode());
        System.out.println(rectangle.toString());


    }



}

