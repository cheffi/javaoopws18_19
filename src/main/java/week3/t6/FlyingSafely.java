package week3.t6;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Flying Safely
 * Link:https://open.kattis.com/problems/flyingsafely
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28.11.2018
 * Method:Ad-Hoc
 * Status:Accepted
 * Runtime:0.62s
 */

public class FlyingSafely {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int testcases = Integer.parseInt(input.nextLine());
        for (int j = 0; j < testcases; j++) {
            String line =input.nextLine();
            String[] secondLine = line.split(" ");
            int cities = Integer.parseInt(secondLine[0]);
            int pilots = Integer.parseInt(secondLine[1]);
            for (int i = 0; i < pilots; i++) {
                input.nextLine();
            }
            System.out.println(cities - 1);
        }

    }





}


