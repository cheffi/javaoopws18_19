package week3.t6;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Heliocentric
 * Link: https://open.kattis.com/contests/pp5rtp/problems/heliocentric;
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 11/05/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.08s
 */
import java.util.Scanner;

public class Heliocentric {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = 1;

        while (scanner.hasNext()) {

            int e = scanner.nextInt();
            int m = scanner.nextInt();
            int d = 0;

            while (!(e == 0 && m == 0)) {

                e = ++e % 365;
                m = ++m % 687;
                //count days until 0
                d++;
            }

            System.out.println("Case " + n + ": " + d);
            n++;
        }
    }
}