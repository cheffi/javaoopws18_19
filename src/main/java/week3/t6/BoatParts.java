
package week3.t6;
/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Boat Parts
 * Link:https://open.kattis.com/problems/boatparts
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28.11.2018
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime:0.10s
 */

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;


public class BoatParts {

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        String firstLine = input.nextLine();
        int boatPart = Integer.parseInt(firstLine.split(" ")[0]);
        int day = Integer.parseInt(firstLine.split(" ")[1]);
        Set<String> wordList = new HashSet<>();
        for(int i=1;i <= day; i++){
          wordList.add(input.nextLine());
         if(wordList.size() == boatPart) {
             System.out.println(i);
             return;
         }

        }

        System.out.println("paradox avoided");


    }

}

