package week3.t6;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Left Beehind
 * Link: https://open.kattis.com/contests/pp5rtp/problems/leftbehind;
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 11/01/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.08s
 */
import java.util.Scanner;

public class LeftBeehind {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        while(true) {
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            if(x == 0 && y == 0) {
                break;
            }

            if (x + y == 13) {
                System.out.println("Never speak again.");
            }else if (x == y)
                System.out.println("Undecided.");
            else if (x > y) {
                System.out.println("To the convention.");
            }else {
                System.out.println("Left beehind.");
            }
        }
    }
}
