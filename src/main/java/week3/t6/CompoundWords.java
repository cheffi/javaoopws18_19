package week3.t6;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Compound Words
 * Link: https://open.kattis.com/contests/pp5rtp/problems/compoundwords;
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 11/01/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.08s
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class CompoundWords {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        ArrayList<String> list = new ArrayList<>();
        ArrayList<String> resultList = new ArrayList<>();


        while (true) {
            if(!scanner.hasNext()) {
                break;
            }
            list.add(scanner.next());
        }

        //for each index i add all possible index x values from the input
        //no value will be set twice
        for (int i = 0; i < list.size(); i++) {
            for (int j = i + 1; j < list.size(); j++) {

                //if the value is not in the list -> add it to resultList
                if (!resultList.contains(list.get(i) + list.get(j))) {
                    resultList.add(list.get(i) + list.get(j));
                }

                if (!resultList.contains(list.get(j) + list.get(i))) {
                    resultList.add(list.get(j) + list.get(i));
                }
            }
        }
        Collections.sort(resultList);

        for (int i = 0; i < resultList.size(); i++) {
            System.out.println(resultList.get(i));
        }
    }
}
