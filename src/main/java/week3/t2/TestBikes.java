package week3.t2;

import week2.t2.Bicycle;

public class TestBikes {

    public static void main(String[] args) {

        Bicycle bike01, bike02, bike03;

        bike01 = new Bicycle(20, 10, 1);
        bike02 = new MountainBike(20, 10, 5, "Dual");
        bike03 = new RoadBike(40, 20, 8, 23);

        bike01.printDescription();
        bike02.printDescription();
        bike03.printDescription();

        System.out.println(bike01.hashCode());
        System.out.println(bike01.getClass().getTypeName());
        if (bike01.equals(bike02))
            System.out.println("Bike 1 is Equal to Bike 2:");
        else {
            System.out.printf(bike01.getClass().getSimpleName() + " is not " + bike02.getClass().getSimpleName());

        }
        if (bike01 instanceof MountainBike) {
            Bicycle o = (RoadBike) bike01;
            System.out.println("o is now Casted to :  " + o);


        }


    }
}
