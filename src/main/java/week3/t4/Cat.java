package week3.t4;

public class Cat extends Animal {

    public static void testClassMethod() {
        System.out.println("The static method in Cat");
    }

    public void testInstanceMethod() {
        System.out.println("The instance method in Cat");
    }

    public static void main(String[] args) {
        Cat myCat = new Cat();
        Animal.testClassMethod();
        ((Animal) myCat).testInstanceMethod();
        myCat.testInstanceMethod();
        System.out.println(((Animal) myCat).getClass().hashCode());
        System.out.println(((Animal) myCat).hashCode());


    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
