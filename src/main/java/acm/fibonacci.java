package acm;

import java.util.Scanner;

public class fibonacci {

    static int FIB(int n){
        if (n<= 1)
            return n;
        else{
            int x = FIB(n-1);
            int y = FIB(n-2);
            return x+y;
        }
    }
    
    public static void main(String[] args) {
     int n = 3;
        Scanner scanner = new Scanner(System.in);
        System.out.println(FIB(n));
    }
        
}
