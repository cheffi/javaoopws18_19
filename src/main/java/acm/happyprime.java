package acm;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem: Happy Happy Prime Prime
 * Link: https://open.kattis.com/problems/happyprime
 * @author Kevin Kopp
 * @version 1.0, 10/11/2019
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.52s
 */
public class happyprime {

    static boolean isPrime(int n) {
        if (n == 1)
            return false;
        for (int i = 2; i < n; i++) {
            if (n % i == 0)
                return false;
        }
        return true;
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int p, k, m;

        int number = 0, n;


        p = scanner.nextInt();


        while (p > 0) {
            k = scanner.nextInt();
            m = scanner.nextInt();
            boolean ok = isPrime(m);
            if (!ok) {
                System.out.println(k + " " + m + " NO ");

            } else {
                n = m;
                while (true) {
                    while (n > 0) {
                        number += (int) Math.pow((n % 10), 2);
                        n = n / 10;
                    }
                    n = number;
                    number = 0;
                    if (n == 89 || n == 145) {
                        ok = false;
                        break;
                    }
                    if (n == 1) {
                        ok = true;
                        break;
                    }
                }
                System.out.println(ok ? k + " " + m + " YES " : k + " " + m + " NO ");
            }

            p--;
        }

    }
}
