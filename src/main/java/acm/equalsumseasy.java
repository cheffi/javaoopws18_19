package acm;

import java.util.*;

public class equalsumseasy {

    static boolean isSubsetSum(int set[],
                               int n, int sum) {
        // Base Cases 
        if (sum == 0)
            return true;
        if (n == 0 && sum != 0)
            return false;

        // If last element is greater than  
        // sum, then ignore it 
        if (set[n - 1] > sum)
            return isSubsetSum(set, n - 1, sum); 
          
        /* else, check if sum can be obtained  
        by any of the following 
            (a) including the last element 
            (b) excluding the last element */
        return isSubsetSum(set, n - 1, sum) ||
                isSubsetSum(set, n - 1, sum - set[n - 1]);
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int testcases = scanner.nextInt();
        int count = 1;
        while (testcases-- > 0) {

            int size = scanner.nextInt();
            List<Integer> numbers = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                int x = scanner.nextInt();
                numbers.add(x);
            }

        }

    }

}
