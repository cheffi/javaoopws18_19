package acm;

import java.util.Scanner;

public class ratingproblems {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n, k;

        n = scanner.nextInt();
        k = scanner.nextInt();
        double max, min;
        int unrated = n-k;

        int rate = 0;
        for (int i = 0; i < k; i++) {
             rate += scanner.nextInt();
        }
        max =(double) (3*unrated+rate)/n;
        min =(double) (-(3*unrated)+rate)/n;
        System.out.println(min +" "+ max);
    }

}
