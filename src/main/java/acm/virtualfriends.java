package acm;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class virtualfriends {

    static String find() {

        String findd = null;
        return findd;
    }
        static void union (String firstFriend, String secondFriend){


        }

        public static void main (String[]args){
            Scanner scanner = new Scanner(System.in);

            int testcases = scanner.nextInt();
            String firstFriend, secondFriend;
            Set<String> firstUnion;
            while (testcases > 0) {
                firstUnion = new TreeSet<>();
                int friendShip = scanner.nextInt();

                while (friendShip > 0) {
                    firstFriend = scanner.next();
                    secondFriend = scanner.next();
                    firstUnion.add(firstFriend);
                    firstUnion.add(secondFriend);
                    System.out.println(firstUnion.size());

                    friendShip--;
                }


                testcases--;

            }
        }

        class DisjointUnionSets {
            int[] rank, parent;
            int n;

            // Constructor 
            public DisjointUnionSets(int n) {
                rank = new int[n];
                parent = new int[n];
                this.n = n;
                makeSet();
            }

            // Creates n sets with single item in each 
            void makeSet() {
                for (int i = 0; i < n; i++) {
                    // Initially, all elements are in 
                    // their own set. 
                    parent[i] = i;
                }
            }

            // Returns representative of x's set 
            int find(int x) {
                // Finds the representative of the set 
                // that x is an element of 
                if (parent[x] != x) {
                    // if x is not the parent of itself 
                    // Then x is not the representative of 
                    // his set, 
                    parent[x] = find(parent[x]);

                    // so we recursively call Find on its parent 
                    // and move i's node directly under the 
                    // representative of this set 
                }

                return parent[x];
            }

            // Unites the set that includes x and the set 
            // that includes x 
            void union(int x, int y) {
                // Find representatives of two sets 
                int xRoot = find(x), yRoot = find(y);

                // Elements are in the same set, no need 
                // to unite anything. 
                if (xRoot == yRoot)
                    return;

                // If x's rank is less than y's rank 
                if (rank[xRoot] < rank[yRoot])

                    // Then move x under y  so that depth 
                    // of tree remains less 
                    parent[xRoot] = yRoot;

                    // Else if y's rank is less than x's rank 
                else if (rank[yRoot] < rank[xRoot])

                    // Then move y under x so that depth of 
                    // tree remains less 
                    parent[yRoot] = xRoot;

                else // if ranks are the same 
                {
                    // Then move y under x (doesn't matter 
                    // which one goes where) 
                    parent[yRoot] = xRoot;

                    // And increment the result tree's 
                    // rank by 1 
                    rank[xRoot] = rank[xRoot] + 1;
                }
            }
        }
    }
  

