package acm;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem:
 * Link: https://open.kattis.com/problems/hittingthetargets
 *
 * @author Kevin Kopp
 * @version 1.0, M/DD/YYYY
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.16 s
 */
public class hittingthetargets {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<Integer> Rectangles = new ArrayList<>();
        ArrayList<Integer> Circles = new ArrayList<>();

        int targets = scanner.nextInt();
        String rec = "rectangle";
        String cir = "circle";
        for (int i = 0; i < targets; i++) {
            String str = scanner.next();

            if (str.equals(rec)) {
                IntStream.range(0, 4).mapToObj(z -> scanner.nextInt()).forEach(Rectangles::add);
            }
            if (str.equals(cir)) {
                for (int z = 0; z < 3; z++)
                    Circles.add(scanner.nextInt());
            }
        }

        int testcases = scanner.nextInt();

        int i = 0;
        while (i < testcases) {
            int x = scanner.nextInt();
            int y = scanner.nextInt();

            int shots = 0;

            {
                int j = 0;
                while (j < Rectangles.size()) {
                    if (x >= Rectangles.get(j) && y >= Rectangles.get(j + 1) && x <= Rectangles.get(j + 2) && y <= Rectangles.get(j + 3))
                        shots++;
                    j += 4;
                }
            }

            int j = 0;
            while (j < Circles.size()) {
                if (Math.sqrt((x - Circles.get(j)) * (x - Circles.get(j)) + (y - Circles.get(j + 1)) * (y - Circles.get(j + 1))) <= Circles.get(j + 2))
                    shots++;
                j += 3;
            }

            System.out.println(shots);
            i++;
        }


    }
}