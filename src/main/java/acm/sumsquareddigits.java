package acm;

import java.util.Scanner;

public class sumsquareddigits {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);

        int testcases = scanner.nextInt();


        while (testcases > 0) {
            int count = scanner.nextInt();
            int b = scanner.nextInt();
            int digits = scanner.nextInt();

            int res = 0;
            while (digits>0){
                res += (digits % b)* (digits % b);
                digits /= b;
            }
            System.out.println(count + " "+ res);

            testcases--;
        }

    }

}
