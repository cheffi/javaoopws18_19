package acm;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem: ACM Contest Scoring
 * Link: https://open.kattis.com/problems/acm
 * @author Kevin Kopp
 * @version 1.0, 11/22/2019
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.18s
 */
public class acm {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<String> result = new ArrayList<>();
        List<Integer> times = new ArrayList<>();
        List<Character> problems = new ArrayList<>();


        int sumOfSolvedProblems = 0;
        int rating = 0;
        while (scanner.hasNextLine()) {
            String[] logs = scanner.nextLine().split(" ");
            if (-1 == Integer.parseInt(logs[0])) break;
            times.add(Integer.parseInt(logs[0]));
            problems.add(logs[1].charAt(0));
            result.add(logs[2]);


        }
        sumOfSolvedProblems = getSumOfSolvedProblems(result);
        String res = getAllSolvedProblems(result, problems);

        rating = sumRatingOfTeam(res, times, problems, result);
        System.out.println(sumOfSolvedProblems + " " + rating);

    }

    static int sumRatingOfTeam(String res, List<Integer> t, List<Character> characters, List<String> result) {
        int rating = 0;


        for (int i = 0; i < res.length(); i++) {
            char c = res.charAt(i);
            for (int j = 0; j < characters.size(); j++) {
                if (c == characters.get(j)) {
                    if (result.get(j).contentEquals("wrong"))
                        rating += 20;
                    else
                        rating += t.get(j);
                }
            }

        }


        return rating;

    }

    static int getSumOfSolvedProblems(List<String> e) {

        return (int) e.stream()
                .filter(s -> s.endsWith("right"))
                .count();
    }

    static String getAllSolvedProblems(List<String> e, List<Character> c) {

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < e.size(); i++) {
            if ("right".equalsIgnoreCase(e.get(i))) {
                result.append(c.get(i).toString());
            }
        }

        return result.toString();
    }

}
