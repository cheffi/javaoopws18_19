package acm;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem: Words for Numbers
 * Link: https://open.kattis.com/problems/wordsfornumbers
 * @author Kevin Kopp
 * @version 1.0, 11/23/2019
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.17s
 */
public class wordsfornumbers {
    static String[] words = new String[]{
            "zero", "one", "two", "three", "four", "five", "six", "seven",
            "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen",
            "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty",
            "twenty-one", "twenty-two", "twenty-three", "twenty-four", "twenty-five", "twenty-six", "twenty-seven", "twenty-eight", "twenty-nine", "thirty",
            "thirty-one", "thirty-two", "thirty-three", "thirty-four", "thirty-five", "thirty-six", "thirty-seven", "thirty-eight", "thirty-nine", "forty",
            "forty-one", "forty-two", "forty-three", "forty-four", "forty-five", "forty-six", "forty-seven", "forty-eight", "forty-nine", "fifty",
            "fifty-one", "fifty-two", "fifty-three", "fifty-four", "fifty-five", "fifty-six", "fifty-seven", "fifty-eight", "fifty-nine", "sixty",
            "sixty-one", "sixty-two", "sixty-three", "sixty-four", "sixty-five", "sixty-six", "sixty-seven", "sixty-eight", "sixty-nine", "seventy",
            "seventy-one", "seventy-two", "seventy-three", "seventy-four", "seventy-five", "seventy-six", "seventy-seven", "seventy-eight", "seventy-nine", "eighty",
            "eighty-one", "eighty-two", "eighty-three", "eighty-four", "eighty-five", "eighty-six", "eighty-seven", "eighty-eight", "eighty-nine", "ninety",
            "ninety-one", "ninety-two", "ninety-three", "ninety-four", "ninety-five", "ninety-six", "ninety-seven", "ninety-eight", "ninety-nine",
    };


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        HashMap<String, String> map = new HashMap<>();
        int count = 0;
        for (String s : words) {
            map.put(String.valueOf(count++), s);
        }
        while (scanner.hasNextLine()) {
            String s = scanner.nextLine();
            String[] sentence = s.split("\\s+");
            for (int i = 0; i < sentence.length; i++) {
                StringBuilder stringBuilder = new StringBuilder(sentence[i]);
                if (map.containsKey(sentence[i])) {
                    if (i == 0) {
                        stringBuilder.setCharAt(0,Character.toUpperCase(map.get(sentence[i]).charAt(0)));
                        stringBuilder.replace(1,sentence[i].length(),map.get(sentence[i]));
                        stringBuilder.deleteCharAt(1);
                        sentence[i] = stringBuilder.toString();
                    } else {
                        sentence[i] = map.get(sentence[i]);
                    }
                }
            }
            for (String f : sentence) {
                System.out.print(f + " ");
            }
        }

    }
}
