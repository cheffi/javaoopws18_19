package acm;

public class fakultaet {
    static int COUNT_ITER = 0;
    static int COUNT_REK = 0;

    public static void main(String[] args) {

        System.out.println(fakIter(5));
        System.out.println(fakRek(5));
        System.out.println("Die Rekursive Variante hat :" + COUNT_REK + "gerechnet");
        System.out.println("Die Iterative Variante hat :" + COUNT_ITER + "gerechnet");
        
    }

    static int fakRek(int n) {
        if (n == 0)
            return 1;
        else {
            COUNT_REK++;
            return fakRek(n - 1) * n;
        }
    }

    static int fakIter(int n) {
        int k = n, i = 1;
        while (k > 1) {
            i = k * i;
            COUNT_ITER++;
            k--;
            COUNT_ITER++;
        }
        return i;
    }
}
