package acm;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Apaxiaaaaaaaaaaaans!
 * Link: https://open.kattis.com/problems/apaxiaaans
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime: 0.07s
 */


import java.util.Scanner;


public class apaxiaaans {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        String text;
        text = input.nextLine();
        StringBuilder text2 = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if ((i == text.length() - 1) || text.charAt(i) != text.charAt(i + 1)) {
                text2.append(text.charAt(i));
            }

        }
        System.out.println(text2);
    }
}