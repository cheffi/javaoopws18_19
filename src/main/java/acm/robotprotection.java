package acm;

import java.util.Scanner;

class Point {
    public int x;
    public int y;

    //constructor
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }


    public Point(Point p) {
        this(p.x, p.y);
    }

    public Point relTo(Point p) {
        return new Point(x - p.x, y - p.y);
    }

    public void makeRelTo(Point p) {
        x -= p.x;
        y -= p.y;
    }


    public Point reversed() {
        return new Point(-x, -y);
    }

    public boolean isLower(Point p) {
        return y < p.y || y == p.y && x < p.x;
    }

    public double mdist()   // Manhattan-Distanz
    {
        return Math.abs(x) + Math.abs(y);
    }

    public double mdist(Point p) {
        return relTo(p).mdist();
    }

    public boolean isFurther(Point p) {
        return mdist() > p.mdist();
    }

    public boolean isBetween(Point p0, Point p1) {
        return p0.mdist(p1) >= mdist(p0) + mdist(p1);
    }

    public double cross(Point p) {
        return x * p.y - p.x * y;
    }

    public boolean isLess(Point p) {
        double f = cross(p);
        return f > 0 || f == 0 && isFurther(p);
    }

    public double area2(Point p0, Point p1) {
        return p0.relTo(this).cross(p1.relTo(this));
    }


    public boolean isConvex(Point p0, Point p1) {
        double f = area2(p0, p1);
        return f < 0 || f == 0 && !isBetween(p0, p1);
    }

}    // end class acm.Point


public class robotprotection {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int maxConfig = 30;
        while (maxConfig-- > 0) {
            int n = scanner.nextInt();
            scanner.nextLine();
            while (n > 0) {

//            int[] xk = new int[n];
//            int[] yk = new int[n];
                Point[] points = new Point[n];
                for (int i = 0; i < n; i++) {
                    String line = scanner.nextLine();
                    String[] input = line.split(" ");
                    int x = Integer.parseInt(input[0]);
                    int y = Integer.parseInt(input[1]);
                    Point p = new Point(x, y);
                    points[i] = p;
//                xk[i] = Integer.parseInt(input[0]);    
//                yk[i] = Integer.parseInt(input[1]);    
                }
                GrahamScan grahamScan = new GrahamScan();
                int h = grahamScan.computeHull(points);
                System.out.println(h);
                n = scanner.nextInt();

            }
        }

    }
}
