package acm;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem: Sticky Situation
 * Link: https://open.kattis.com/problems/stickysituation
 * @author Kevin Kopp
 * @version 1.0, 16/01/2020
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.37s
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class StickySituation {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        ArrayList<Long> list = new ArrayList<>();
        String output = "impossible";

        int n = scanner.nextInt();

        //add all sticks to list
        for (int i = 0; i < n; i++) {
            list.add(scanner.nextLong());
        }
        //sort to compare index i with i + 1
        Collections.sort(list);

        //if the first and second stick is greater than the third -> possible
        for (int i = 0; i < n - 2; i++) {
            if (list.get(i) + list.get(i + 1) > list.get(i + 2)) {
                output = "possible";
                break;
            }
        }
        System.out.println(output);
    }
}
