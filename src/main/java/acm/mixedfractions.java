package acm;

import java.util.Scanner;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem: Mixed Fractions
 * Link: https://open.kattis.com/problems/mixedfractions
 * @author Kevin Kopp
 * @version 1.0, 11/22/2019
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.29s
 */
public class mixedfractions {
    
    
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {

            int numeratior = scanner.nextInt();
            int denumerator = scanner.nextInt();
            if (denumerator>0) {
                System.out.println((numeratior / denumerator) + " " + (numeratior % denumerator) + " / " + denumerator);
            }
            
        }
    }
}
