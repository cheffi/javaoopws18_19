package acm;

import java.util.Scanner;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem: Goldbachs Conjecture
 * Link: https://open.kattis.com/problems/goldbach2
 * @author Kevin Kopp
 * @version 1.0,11/11/2019
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0,35s
 */
public class goldbach2 {

    static int[] eratosthenes(int max) {
        final int maxprim = (int) Math.sqrt(max) + 2;
        boolean[] zahlen = new boolean[max]; 
        for (int i = 0; i < max; i++) 
            zahlen[i] = i % 2 == 1; 

        for (int prim = 3; prim < maxprim; prim += 2) 
            if (zahlen[prim]) { 
                for (int i = prim; i <= max / prim; i++) {
                    final int zahl = i * prim;
                    if (zahl < max) 
                        zahlen[zahl] = false;
                }
            }

        int anzahl = 0;
        for (boolean istPrim : zahlen)
            if (istPrim)
                anzahl++;
        int[] primzahlen = new int[anzahl];
        int index = 0;
        for (int i = 0; i < zahlen.length; i++)
            if (zahlen[i])
                primzahlen[index++] = i;
        primzahlen[0] = 2; 
        return primzahlen;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testcases = scanner.nextInt();
        StringBuilder result;
        while (testcases > 0) {
            int counter = 0;
            String sumOne, sumTwo;
            int x, j, i = 0;
            x = scanner.nextInt();
            int[] primNumbersInRange = eratosthenes(x);

            result = new StringBuilder();
            j = primNumbersInRange.length - 1;


            while (i <= j) {

                int res = primNumbersInRange[i] + primNumbersInRange[j];
                sumOne = String.valueOf(primNumbersInRange[i]);
                sumTwo = String.valueOf(primNumbersInRange[j]);
                if (res > x) {
                    j--;
                }
                if (x == res) {
                    i++;
                    j--;
                    counter++;
                    result.append("\n").append(sumOne).append("+").append(sumTwo);
                }
                if (res < x) {
                    i++;
                }

            }
            System.out.println(x + " has " + counter + " representation(s)" + result.toString() + "\n");

            testcases--;
        }

    }
}
