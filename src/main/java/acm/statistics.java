package acm;

import java.io.Console;
import java.util.*;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem:
 * Link:
 *
 * @author Kevin Kopp
 * @version 1.0,
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime:
 */
public class statistics {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        
        
        int maxTestCase = 1;



        while (scanner.hasNextInt()) {
            
            SortedSet<Integer> sample = new TreeSet<>();
            int sampleCount = scanner.nextInt();
            for (int i = 0; i < sampleCount; i++) {
                int number = scanner.nextInt();
                sample.add(number);
            }
            maxTestCase++;
            System.out.println("Case "+(maxTestCase - 1)+": "+  + sample.first() + " " + sample.last() + " " + (sample.last() - sample.first()));

        }
         scanner.close();
    }
}
