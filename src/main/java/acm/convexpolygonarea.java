package acm;

import java.util.Scanner;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem: Convex Polygon Area
 * Link: https://open.kattis.com/problems/convexpolygonarea
 * @author Kevin Kopp
 * @version 1.0, 12/18/2019
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime :0.27s
 */
public class convexpolygonarea {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numberOfPolygons = scanner.nextInt();
        while (numberOfPolygons-- > 0) {
            int anzPoints = scanner.nextInt();
            double xk[] = new double[anzPoints];
            double yk[] = new double[anzPoints];
            for (int i = 0; i < anzPoints; i++) {
                int x = scanner.nextInt();
                int y = scanner.nextInt();
                xk[i] = x;
                yk[i] = y;
            }
            double area = berechnePolygonFlaeche(xk,yk);
            System.out.println(area);
        }

    }

    public static double berechnePolygonFlaeche(double[] x, double[] y) {
        if ((x == null) || (y == null)) return 0.0;  // auf leere Argumente testen
        int n = Math.min(x.length, y.length);        // Anzahl der Ecken des Polygons
        if (n < 3) return 0.0;                       // ein Polygon hat mindestens drei Eckpunkte
        double a = 0.0;                              // Variable fuer Flaeche des Polygons
        for (int i = 0; i < n; i++) {                // Schleife zwecks Summenbildung
            a += (y[i] + y[(i + 1) % n]) * (x[i] - x[(i + 1) % n]);
        }
        return Math.abs(a / 2.0);                    // Flaecheninhalt zurueckliefern
    }
}
