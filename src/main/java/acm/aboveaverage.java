package acm;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class aboveaverage {
    
    
    
    
    public static void main(String[] args) {

        int testcases;
        Scanner scanner = new Scanner(System.in);
        testcases = scanner.nextInt();
        for (int i = 0; i < testcases; i++) {
            int sum = 0;
            double avg = 0;
            int count = 0;

            int numOfPeople = scanner.nextInt();
            List<Integer> peoples = new ArrayList<>(numOfPeople);

            for (int j = 0; j < numOfPeople; j++) {
                int grade = scanner.nextInt();
                peoples.add(grade);

            }
            for (int p : peoples) {
                sum += p;
                avg = (double) sum/numOfPeople;
            }
            for (int j = 0; j < numOfPeople; j++) {
                if (peoples.get(j) > avg)
                    count++;

            }
            double percentage = (double)count/numOfPeople;
            System.out.printf("%.3f%s \n", percentage * 100, "%");
   

        }


    }

}

