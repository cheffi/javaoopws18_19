package acm;

import java.util.Scanner;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem:
 * Link:
 *
 * @author Kevin Kopp
 * @version 1.0,
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime:
 */
public class apaxianparent {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String ex = "ex";
        StringBuilder stringBuilder = new StringBuilder();


        String y = scanner.next();
        String p = scanner.next();

        if (y.endsWith("e")) {
            stringBuilder.append(y).append("x").append(p);
            System.out.println(stringBuilder.toString());
        } else if (y.endsWith(ex)) {
            stringBuilder.append(y).append(p);
            System.out.println(stringBuilder.toString());

        } else if (y.endsWith("a") || y.endsWith("i") || y.endsWith("o") || y.endsWith("u")) {
            stringBuilder.append(y);
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            stringBuilder.append(ex).append(p);
            System.out.println(stringBuilder.toString());

        }else {
                stringBuilder.append(y);
                stringBuilder.append(ex).append(p);
                System.out.println(stringBuilder.toString());

            }


    }
}
