package acm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class knap {
    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub
        Scanner scanner = new Scanner(System.in);
        String input;
        while ((input = scanner.nextLine()) != null) {

            String[] arActual = input.split(" ");

            double c = Double.parseDouble(arActual[0]);
            int n = Integer.parseInt(arActual[1]);
            int[] val = new int[n];
            int[] weight = new int[n];
            for (int i = 0; i < n; i++) {
                String[] act = scanner.nextLine().split(" ");
                val[i] = Integer.parseInt(act[0]);
                weight[i] = Integer.parseInt(act[1]);
            }
            System.out.println(knapSack(c, weight, val, n));

        }
    }

    static int max(int a, int b) {
        return (a > b) ? a : b;
    }

    // Returns the maximum value that can be put in a knapsack of capacity W
    static int knapSack(double W, int wt[], int val[], int n) {
        int i, w;
        int K[][] = new int[n + 1][(int) (W + 1)];

        // Build table K[][] in bottom up manner
        for (i = 0; i <= n; i++) {
            for (w = 0; w <= W; w++) {
                if (i == 0 || w == 0)
                    K[i][w] = 0;
                else if (wt[i - 1] <= w)
                    K[i][w] = max(val[i - 1] + K[i - 1][w - wt[i - 1]], K[i - 1][w]);
                else
                    K[i][w] = K[i - 1][w];
            }
        }

        return K[n][(int) W];
    }
}

