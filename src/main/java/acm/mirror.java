package acm;

import java.util.Scanner;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem: Mirror Images
 * Link: https://open.kattis.com/problems/mirror
 * @author Kevin Kopp
 * @version 1.0, M/DD/YYYY
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.22s
 */
public class mirror {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testcases, row, colum, count;
        count = 0;
        testcases = scanner.nextInt();
        String[] mir;
        while (testcases > 0) {
            row = scanner.nextInt();
            colum = scanner.nextInt();


            mir = new String[row];

            for (int i = 0; i < row; i++) {
                mir[i] = scanner.next();

            }

            testcases--;
            System.out.println("Test " + ++count);
            String s = "";

            for (int i = mir.length - 1; i >= 0; i--) {
                s = new StringBuilder(mir[i]).reverse().toString();

                System.out.println(s);

            }
        }
    }


}


