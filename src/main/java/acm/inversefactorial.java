package acm;

import java.util.*;

public class inversefactorial {

    public static void main(String[] args) {

        HashMap<Long,Long> Factorial = new HashMap<>(1000001);
        long count = 1,n,fak=1;
        
        for (int i = 1; i < 1000001; i++) {

            fak *= i;
            Factorial.put(count,fak);

        }
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextLong();
        System.out.println(Factorial.get(n));

    }
}
