package acm;
/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem:
 * Link: https://open.kattis.com/problems/honey
 *
 * @author Kevin Kopp
 * @version 1.0, 02.02.2020
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.10s
 */

import week1.t6.Kattio;

import java.util.Arrays;

public class honey {

    public static void main(String[] args) {

        Kattio in = new Kattio(System.in,System.out);

        int n = in.getInt();
        int[] input = new int[n];

        for (int i = 0; i < n; i++) {
            input[i] = in.getInt();
        }

        int max = max(input);
        int[][][] results = new int[2 * max + 1][2 * max + 1][max + 1];

        results[max][max][0] = 1;
        int step = 1;
        while (step < max + 1) {
            int x = 0;
            while (x < 2 * max) {
                int y = 0;
                while (y < 2 * max) {

                    if (y > 0)
                        results[x][y][step] += results[x][y - 1][step - 1];

                    if (x > 0)
                        results[x][y][step] += results[x - 1][y][step - 1];

                    if (x > 0 && y > 0)
                        results[x][y][step] += results[x - 1][y - 1][step - 1];

                    if (x < 2 * max + 1)
                        results[x][y][step] += results[x + 1][y][step - 1];
                    if (y < 2 * max + 1)
                        results[x][y][step] += results[x][y + 1][step - 1];
                    if (x < 2 * max + 1 && y < 2 * max + 1)
                        results[x][y][step] += results[x + 1][y + 1][step - 1];
                    y++;
                }
                x++;
            }
            step++;
        }
        Arrays.stream(input).map(x -> results[max][max][x]).forEach(System.out::println);

        
    }

    private static int max(int[] input) {
        int max = 0;
        for (int v : input) {
            if (v > max) {
                max = v;
            }
        }
        return max;
    }
}