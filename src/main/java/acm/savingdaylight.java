package acm;

import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem:
 * Link:
 *
 * @author Kevin Kopp
 * @version 1.0,
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime:
 */
public class savingdaylight {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextLine()) {
            String string = scanner.nextLine();
            if (string.isEmpty()) break;
            String[] input = string.split("\\s+");
            String month, day, year;
            LocalTime sunrise, sunset, duration;
            String timeColonPattern = "HH:mm";
            int min, h, min2, min3;
            DateTimeFormatter d = DateTimeFormatter.ofPattern(timeColonPattern);
            month = input[0];
            day = input[1];
            year = input[2];
            String m = input[3];
            String e = input[4];
            StringBuilder stringBuilder = new StringBuilder(m);
            StringBuilder stringBuilder2 = new StringBuilder(e);
            if (m.length() == 4) {
                stringBuilder.insert(0, "0");
            }
            if (e.length() == 4) {
                stringBuilder2.insert(0, "0");

            }

            sunrise = LocalTime.parse(LocalTime.parse(stringBuilder.toString()).format(d));
            sunset = LocalTime.parse(LocalTime.parse(stringBuilder2.toString()).format(d));

            min2 = sunrise.getMinute();
            min3 = sunset.getMinute();
            duration = sunset.minusMinutes(sunrise.getMinute());
            min = min3 - min2;
            duration = sunset.minusHours(sunrise.getHour());
            h = duration.getHour();
            if (min < 0) {
                h--;
                min += 60;
            }
            System.out.println(month + " " + day + " " + year + " " + h + " hours " + min + " minutes ");
        }
        scanner.close();

    }


}
