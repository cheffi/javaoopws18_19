package acm;

import week1.t6.Kattio;

import java.util.Scanner;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem: Pig Latin
 * Link: https://open.kattis.com/problems/piglatin
 * @author Kevin Kopp
 * @version 1.0, 11/21/2019
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.95s
 */
public class piglatin {

    public static void main(String[] args) {
        Kattio scanner = new Kattio(System.in);
        String yay = "yay";
        String ay = "ay";
        while (scanner.hasMoreTokens()) {
            StringBuilder stringBuilder = new StringBuilder();
            String word = scanner.getWord();
            String[] words = word.split("\\s+");
            for (String value : words) {
                if (isVowel(value.charAt(0))) {
                    stringBuilder.append(value).append(yay);
                } else {
                    int idx = indexOfFirstVowel(value);
                    stringBuilder.append(value, idx, value.length());
                    stringBuilder.append(value, 0, idx);
                    stringBuilder.append(ay);
                }
                stringBuilder.append(' ');
            }
            System.out.println(stringBuilder);
        }
        scanner.close();
    }

    private static int indexOfFirstVowel(String word) {
        for (int index = 0; index < word.length(); index++) {
            if (isVowel(word.charAt(index)))
                return index;
        }
        return -1;
    }

    private static boolean isVowel(char c) {
        return c == 'a' || c == 'o' || c == 'e' || c == 'u' || c == 'i' || c == 'y';
    }
}