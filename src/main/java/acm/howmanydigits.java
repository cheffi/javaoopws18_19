package acm;

import week1.t6.Kattio;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem: How Many Digits
 * Link: https://open.kattis.com/problems/howmanydigits
 * @author Kevin Kopp
 * @version 1.0, 4/11/2019
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0,47s
 */
public class howmanydigits {

    public static void main(String[] args) {
        Kattio scanner = new Kattio(System.in, System.out);
        float counter = 1;
        double[] values = new double[1000001];
        values[0] = 0;
        for (int i = 1; i <= 1000000; i++) {
            values[i] = values[i - 1] + Math.log10(i);
        }
        while (scanner.hasMoreTokens()) {
            int number = scanner.getInt();
            System.out.printf("%d \n",(int)values[number]+1);

        }
        scanner.close();
    }

}
