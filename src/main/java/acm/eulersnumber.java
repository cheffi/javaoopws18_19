package acm;

import java.util.Scanner;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem:
 * Link: https://open.kattis.com/problems/eulersnumber
 * @author Kevin Kopp
 * @version 1.0, M/DD/YYYY
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime:
 */
public class eulersnumber {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        long[] faktorial = new long[10001];
        long fak = 1;
        double res = 1;
        for (int i = 1; i <= faktorial.length-1; i++) {

            faktorial[i] = fak *= i;

        }

        for (int i = 1; i <= n; i++) {
            if(i>=17)break;
            res += (double) 1 / faktorial[i];

        }
        System.out.println(res);


    }
}
