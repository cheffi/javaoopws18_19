package acm;

import java.util.Scanner;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem:
 * Link:
 *
 * @author Kevin Kopp
 * @version 1.0,
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime:
 */
public class Avion {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        for (int i = 0; i < 5 ; i++) {
            String registrationCode = scanner.nextLine().toUpperCase();
            if(registrationCode.contains("FBI")) {
                System.out.println(i+1);
            }else {
                count++;
                System.out.print(count == 5 ? "HE GOT AWAY!" : "");
            }
        }


    }
}
