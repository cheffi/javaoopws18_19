package acm;

import java.util.Scanner;

public class numberfun {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        int testcases = scanner.nextInt();
        double a, b, c;
        double mul = 0, div = 0, add = 0, sub = 0, subRev = 0, divRev = 0;


        while (testcases > 0) {
            a = scanner.nextInt();
            b = scanner.nextInt();
            c = scanner.nextInt();
            mul = a * b;
            div = a / b;

            add = a + b;
            sub = a - b;
            subRev = b - a;
            divRev = b / a;

            boolean ok = false;
            if (mul == c)
                ok = true;
            if (div == c)
                ok = true;
            if (add == c)
                ok = true;
            if (subRev == c)
                ok = true;
            if (divRev == c)
                ok = true;
            if (sub == c)
                ok = true;

            System.out.println(ok ? "Possible" : "Impossible");
            ok = false;
            testcases--;

        }

    }


}
        
