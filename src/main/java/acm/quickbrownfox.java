package acm;

import java.util.Scanner;

/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem: Quick Brown Fox
 * Link: https://open.kattis.com/problems/quickbrownfox
 * @author Kevin Kopp
 * @version 1.0, 11/22/2019
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.17s
 */
public class quickbrownfox {
    public static void main(String[] args) {



        Scanner scanner = new Scanner(System.in);
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        int testcases = scanner.nextInt();
        scanner.nextLine();
        while (testcases > 0) {
            StringBuilder missingLetter = new StringBuilder();
            String input = scanner.nextLine().toLowerCase();

            for (int i = 0; i < alphabet.length(); i++)
                if (input.indexOf(alphabet.charAt(i)) < 0)
                    missingLetter.append(alphabet.charAt(i));


            System.out.println(missingLetter.length() == 0 ? "pangram" : "missing " + missingLetter.toString());


            testcases--;
        }


    }
}
