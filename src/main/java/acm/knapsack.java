package acm;//package acm;
//
//import java.util.*;
//              


import java.util.Scanner;

public class knapsack {
    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub
        Scanner scanner = new Scanner(System.in);
        String input;
        while ((input = scanner.nextLine()) != null) {

            String[] arActual = input.split(" ");

            double c = Double.parseDouble(arActual[0]);
            int n = Integer.parseInt(arActual[1]);
            int[] val = new int[n];
            int[] weight = new int[n];
            for (int i = 0; i < n; i++) {
                String[] act = scanner.nextLine().split(" ");
                val[i] = Integer.parseInt(act[0]);
                weight[i] = Integer.parseInt(act[1]);
            }
            System.out.println(knapSack(c, weight, val, n));

        }
    }

    static int max(int a, int b) {
        return Math.max(a, b);
    }

    // Returns the maximum value that can be put in a knapsack of capacity W
    static int knapSack(double W, int wt[], int val[], int n) {
        int i, w;
        int K[][] = new int[n + 1][(int) (W + 1)];

        // Build table K[][] in bottom up manner
        for (i = 0; i <= n; i++) {
            for (w = 0; w <= W; w++) {
                if (i == 0 || w == 0)
                    K[i][w] = 0;
                else if (wt[i - 1] <= w)
                    K[i][w] = max(val[i - 1] + K[i - 1][w - wt[i - 1]], K[i - 1][w]);
                else
                    K[i][w] = K[i - 1][w];
            }
        }

        return K[n][(int) W];
    }
}




























// MY TRIED CODE BEGINS HERE <------ 
//public class knapsack {
//
//
//    static int max(int a, int b) {
//        return Math.max(a, b);
//    }
//
// 
//    static int[][] behalten;
//    static Set<Integer> bag;
//
//    static void create(int numOfObjects, int maxCapacity, List<Integer> weights) {
//        while (numOfObjects-- > 0) {
//            if (behalten[numOfObjects][maxCapacity] == 1) {
//                bag.add(numOfObjects - 1);
//                numOfObjects--;
//                maxCapacity = maxCapacity - weights.get(numOfObjects);
//            }
//        }
//    }
//
//    static int knapSack(int maxCapacity, List<Integer> weights, List<Integer> values, int numberOfObjects, Set<Integer> bag) {
//        int i, w;
//        int K[][] = new int[numberOfObjects + 1][maxCapacity + 1];
//
//        // Build table K[][] in bottom up manner 
//        for (i = 0; i <= numberOfObjects; i++) {
//            for (w = 0; w <= maxCapacity; w++) {
//                if (i == 0 || w == 0) {
//                    K[i][w] = 0;
//                } else if (weights.get(i - 1) <= w) {
//                    K[i][w] = max(values.get(i - 1) + K[i - 1][w - weights.get(i - 1)], K[i - 1][w]);
//                    if (values.get(i - 1) + K[i - 1][w - (weights.get(i - 1))] > K[i - 1][w]) {
//                        behalten[i][w] = 1;
//                    } else {
//                        behalten[i][w] = -1;
//                    }
//                } else {
//                    K[i][w] = K[i - 1][w];
//                    behalten[i][w] = -1;
//
//                }
//            }
//        }
//        return K[numberOfObjects][maxCapacity];
//    }
//
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        while (true) {
//
//            String[] input = scanner.nextLine().split(" ");
//            if (input[0].equals(" "))break;
//            behalten = new int[2002][2002];
//            int maxCapacity = Integer.parseInt(input[0]);
//            int numberOfObjects = Integer.parseInt(input[1]);
//            List<Integer> values = new ArrayList<>(numberOfObjects);
//            List<Integer> weights = new ArrayList<>(numberOfObjects);
//            bag = new HashSet<>();
//            int count = numberOfObjects;
//            while (count-- > 0) {
//                input = scanner.nextLine().split(" ");
//                int value = Integer.parseInt(input[0]);
//                int weight = Integer.parseInt(input[1]);
//                values.add(value);
//                weights.add(weight);
//                if (count==0){
//                    knapSack(maxCapacity, weights, values, numberOfObjects, bag);
//                    create(numberOfObjects,maxCapacity, weights);
//                    bag.forEach(s-> System.out.print(s+" "));
//                }
//
//            }
//
//
//        }
//
////        bag.forEach(s-> System.out.print(s+" ") );
//    }
//    
//    
//}
