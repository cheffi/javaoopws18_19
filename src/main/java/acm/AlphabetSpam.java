package acm;

import java.util.Scanner;
/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Alphabet spam
 * Link: https://open.kattis.com/problems/alphabetspam
 * @author Kevin Kopp
 * @version 1.0, 8/11/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.14 s
 */

public class AlphabetSpam {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int whitespace = 0;
		int lowercase = 0;
		int uppercase = 0;
		int symbols = 0;
		String spam = input.nextLine();
		int i;
		for(i=0; i<spam.length();i++) {
			if(spam.charAt(i)=='_') {
				whitespace++;
			}else if(spam.charAt(i)>='a' && spam.charAt(i)<='z') {
				lowercase++;
			}else if(spam.charAt(i)>='A' && spam.charAt(i)<='Z') {
				uppercase++;
			}else {
				symbols++;
			}
		}
		double n = i ;
		// Every type of character is divided by the number of character in the whole string
		System.out.printf("%f\n%f\n%f\n%f\n", whitespace/n, lowercase/n, uppercase/n, symbols/n);

	}

}
