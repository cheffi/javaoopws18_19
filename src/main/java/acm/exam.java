package acm;

import java.util.Scanner;

public class exam {
    /**
     * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
     * Problem:
     * Link:
     *
     * @author Kevin Kopp
     * @version 1.0,
     * Method : Ad-Hoc
     * Status : Accepted
     * Runtime:
     */
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int numbercorrectanswers = Integer.parseInt(scanner.nextLine().trim());
        String myanswers = scanner.nextLine();
        String friendanswers = scanner.next();
        int equals = 0;
        int diffrent = 0;
        int maxCorrectAnswers = 0;
        int result = 0;

        for (int i = 0; i < myanswers.length(); i++) {
            if (myanswers.charAt(i) == friendanswers.charAt(i))
                equals++;
            else
                diffrent++;
        }

        if (equals > numbercorrectanswers) {

            equals = numbercorrectanswers;
            equals += diffrent;
            System.out.println(equals);

        } else {
            result = myanswers.length() - (numbercorrectanswers - equals);
            System.out.println(result);
        }


    }

}