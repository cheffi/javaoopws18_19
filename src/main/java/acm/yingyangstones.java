package acm;

import java.util.Scanner;


/**
 * Ausgewählte Probleme aus dem ACM Programming Contest WS19/20
 * Problem: Ying Yang Stones
 * Link: https://open.kattis.com/problems/yingyangstones
 * @author Kevin Kopp
 * @version 1.0, 11/22/2019
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.22s
 */
public class yingyangstones {


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        String blackWhite = scanner.nextLine();
        int countW, countB;
        countB = (int) blackWhite.chars().filter(ch -> ch == 'B').count();
        countW = (int) blackWhite.chars().filter(ch -> ch == 'W').count();

        System.out.println(countB == countW ? 1 : 0);

    }

}
