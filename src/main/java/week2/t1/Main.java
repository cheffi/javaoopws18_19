package week2.t1;


public class Main {

    public static void main(String[] args) {
        Point punkt = new Point(5, 10);

        Rectangle rechteck = new Rectangle();
        rechteck.origin = punkt;
        System.out.println(rechteck.ausgabe());

        Rectangle rechteckMitpunkt = new Rectangle(punkt);
        rechteckMitpunkt.move(20,20);
        rechteckMitpunkt.height = 10;
        rechteckMitpunkt.width = 6;
        System.out.println(rechteckMitpunkt.ausgabe());

        Rectangle rechteckMitKoordinate = new Rectangle(1,2);
        System.out.println(rechteckMitKoordinate.ausgabe());

        Rectangle rechteckMitPunktUndKoordinaten = new Rectangle(punkt, 3, 3);
        System.out.println(rechteckMitPunktUndKoordinaten.ausgabe());

        rechteckMitpunkt.move(5,5);
        System.out.println("Neues Rechteck mit Punktobjekt \n"+ rechteckMitpunkt.ausgabe());

    }//main
}//Class