package week2.t4;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Railroad
 * Link:https://open.kattis.com/problems/railroad2
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method :Ad-Hoc
 * Status: Accepted
 * Runtime:0.08s
 */

public class railroad2 {

    public static boolean IstGerade(int y) {
        boolean ok;
        ok = y % 2 == 0;
        return ok;
    }

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int x = scan.nextInt();
        int y = scan.nextInt();
        boolean ok;

        if (IstGerade(y)) {
            System.out.println("possible");
        } else System.out.println("impossible");

    }

}
