package week2.t4;

import java.util.Scanner;
/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Number fun
 * Link: https://open.kattis.com/problems/numberfun
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 25/10/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.35 s
 */
public class NumberFun {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		for (int i = 0; i < n; i++) {
			boolean possible = false;
			int a = input.nextInt();
			int b = input.nextInt();
			int c = input.nextInt();
			int d = a / b;
			int e = b / a;
			if (a + b == c) {
				possible = true;
			} else if (a - b == c || b - a == c) {
				possible = true;
			} else if (a * b == c) {
				possible = true;
			} else if (d == c && d * b == a) {
				possible = true;
			} else if (e == c && e * a == b) {
				possible = true;

			}

			if (possible) {
				System.out.println("Possible");
			} else {
				System.out.println("Impossible");
			}

		}

	}

}
