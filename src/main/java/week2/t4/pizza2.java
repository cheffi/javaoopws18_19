package week2.t4;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * ProblemID: Pizza Crust
 * Link :https://open.kattis.com/users/kkopp/submissions/pizza2
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 31.10.2018
 * Method : Ad-Hoc
 * Status : Accepted
 * Runtime: 0.08s
 */

public class pizza2 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int r = input.nextInt();
        int c = input.nextInt();

        double kaeseinprozent = (double) (r - c) * (r - c) / r / r;
        System.out.format("%.6f", kaeseinprozent * 100);

    }

}
