package week2.t4;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Speed Limit
 * Link:https://open.kattis.com/problems/speedlimit
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime:0.08s
 */


public class Speedlimit {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (true) {
            int numTestCases = scan.nextInt();
            if (numTestCases == -1) return;
            int prev = 0;
            int result = 0;
            for (int testCase = 0; testCase < numTestCases; testCase++) {
                int miles = Integer.parseInt(scan.next());
                int elapsedTime = Integer.parseInt(scan.next());
                result += miles * (elapsedTime - prev);
                prev = elapsedTime;
            }
            System.out.println(result + " miles");
        }
    }
}