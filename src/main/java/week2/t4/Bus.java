package week2.t4;
import java.util.Scanner;
/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Bus
 * Link: https://open.kattis.com/problems/bus
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 25/10/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.08 s
 */
public class Bus {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		for(int i=0;i<n;i++){
	           double p =0; 
	           int m=input.nextInt();
	           for(int j=0;j<m;j++)
	        	   p=(p+.5)*2;
	           System.out.println((int)p);
	        }
	}

}
