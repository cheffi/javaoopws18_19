package week2.t4;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: plania.java
 * Link:
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime:
 */

public class plania {


    public static int algorek(int x) {
        int startpos = 2;

        for (int i = 0; i < x; i++) {
            startpos = startpos + startpos - 1;
        }
        return (startpos * startpos);

    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int ergebnis;
        ergebnis = algorek(n);
        System.out.println(ergebnis);
    }

}
