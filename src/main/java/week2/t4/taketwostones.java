package week2.t4;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: taketwostones.java
 * Link:https://open.kattis.com/problems/twostones
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime:0.07
 */


import java.util.Scanner;


public class taketwostones {
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);


        int isEven = scan.nextInt();

        if (isEven % 2 == 0) {
            System.out.println("Bob");
        } else
            System.out.println("Alice");

    }
}