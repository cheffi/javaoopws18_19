package week2.t4;

/**
 * Object Oriented Programming with Java, WS 2018/19
 * Problem: Paul Eigon
 * Link: https://open.kattis.com/contests/pp5rtp/problems/pauleigon;
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 10/28/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.07s
 */

import java.util.Scanner;

public class PaulEigon {

    public static void main(String[] args) {

        //first -> number of serves
        //second -> pauls current score
        //third opponent current score
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int p = scanner.nextInt();
        int q = scanner.nextInt();

        //the sum of both scores
        int sum = p + q;

        //the result is the amount of rows
        int result = sum / n;

        //if the current row is an odd number -> opponent
        //because paul always starts the game
        if (result % 2 != 0) {
            System.out.println("opponent");
        } else {
            System.out.println("paul");
        }
    }
}