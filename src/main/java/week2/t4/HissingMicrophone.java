package week2.t4;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Hissing microphone
 * Link: https://open.kattis.com/problems/hissingmicrophone
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 25/10/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.07 s
 */

public class HissingMicrophone {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Scanner input = new Scanner(System.in);
        String word = input.next();
        boolean hiss = false;
        for (int i = 0; i < word.length() - 1; i++) {
            // starting from the fist char it chechs if the next one is also an 's'
            if (word.charAt(i) == 's' && word.charAt(i + 1) == 's') {
                hiss = true;
                break;
            }

        }
        if (hiss) {
            System.out.print("hiss");

        } else {
            System.out.print("no hiss");
        }
    }

}
