package week2.t4;

/**
 * Object Oriented Programming with Java, WS 2018/19
 * Problem: Nasty Hacks
 * Link: https://open.kattis.com/contests/pp5rtp/problems/nastyhacks;
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 10/25/2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime: 0.09s
 */
import java.util.Scanner;

public class NastyHacks {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int array[][] = new int[n][3];
        String result[] = new String[n];


        for(int i = 0; i < n; i++) {
            for(int j = 0; j < 3; j++) {
                array[i][j] = scanner.nextInt();
            }
        }

        //first -> revenue without advertising
        //second -> revenue with advertising
        //third -> cost for advertising
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < 3; j++) {
                if(array[i][0] < (array[i][1] - array[i][2])) {
                    result[i] = "advertise";

                }else if(array[i][0] == (array[i][1] - array[i][2])) {
                    result[i] = "does not matter";

                }else {
                    result[i] = "do not advertise";

                }
            }
            System.out.println(result[i]);
        }
    }
}