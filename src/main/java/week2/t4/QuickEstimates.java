package week2.t4;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Quick estimates
 * Link: https://open.kattis.com/problems/quickestimate
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 25/10/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.09 s
 */
public class QuickEstimates {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        for (int i = 0; i < n; i++) {
            String temp = input.next();
            //lenghth of he String
            System.out.println(temp.length());
        }
        input.close();


    }

}
