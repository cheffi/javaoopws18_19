package week2.t4;

/**
 * Object Oriented Programming with Java, WS 2018/19
 * Problem: Parking
 * Link: https://open.kattis.com/contests/pp5rtp/problems/parking2;
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 10/28/2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime: 0.13s
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Parking {

    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<Integer>();
        ArrayList<Integer> result = new ArrayList<Integer>();

        //input:
        //first line ->  number of test cases
        //second -> number of stores
        //third -> position of store
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int next;


        for(int i = 0; i < n; i++) {
            next = scanner.nextInt();
            for(int j = 0; j < next; j++) {
                list.add(scanner.nextInt());
                //sort list to get smallest and greatest number for the position
                Collections.sort(list);
            }
            //result for the distance of (greatest - smallest) * 2
            result.add((list.get(next - 1) - list.get(0)) * 2);
            list.clear();
        }

        for(int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i));
        }
    }
}
