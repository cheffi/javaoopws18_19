package week2.t4;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Sibice
 * Link: https://open.kattis.com/problems/sibice
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 25/10/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime:0.08 s
 */

public class Sibice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input =new Scanner(System.in);
		
		int n = input.nextInt();
		int w= input.nextInt();
		int h= input.nextInt();
		double x = Math.sqrt(w * w + h * h);
		
		 for (int i = 0; i < n; i++) {
			 int l = input.nextInt();
	            if (x >=l) {
	                System.out.println("DA");
	            } else {
	                System.out.println("NE");
	            }
	        }
		
		

	}

}
