package week2.t4;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Grass Seed Inc.
 * Link: https://open.kattis.com/contests/pp5rtp/problems/grassseed;
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 10/25/2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime: 0.09s
 */
import java.util.Scanner;

public class GrassSeed {

    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);

        //cost of seed to sow one square metre of lawn
        String d = scanner.next();
        //the number of lawns to sow
        int n = scanner.nextInt();
        //the width of the lawn, and the length of the lawn
        String array[][] = new String[n][2];

        //result of the cost
        double result = 0.0;


        for(int i = 0; i < n; i++) {
            for(int j = 0; j < 2; j++) {
                array[i][j] = scanner.next();
            }
        }

        //result += index [0] * index [1]
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < 2; j++) {
                result += (Double.parseDouble(array[i][0]) *  (Double.parseDouble(array[i][1])));
            }
        }
        result = result * Double.valueOf(d);


        System.out.println(result / 2);

    }

}