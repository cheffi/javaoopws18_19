package week2.t2;


public class MountainBike extends Bicycle {

    // the MountainBike subclass has
    // one constructor
    public MountainBike(int startCadence,
                        int startSpeed, int startGear) {
        super(startCadence, startSpeed, startGear);
    }

}