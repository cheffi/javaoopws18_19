package week2.t2;

public class Main {

    public static void main(String[] args) {

        // Create two different
        Bicycle myBike = new Bicycle(30, 0, 8);
        Bicycle yourBike = new MountainBike(10, 5, 3);
        // Invoke methods on
        // those objects
        myBike.setCadence(50);
        myBike.speedUp(10);
        myBike.setGear(2);
        myBike.printDescription();
        yourBike.setCadence(50);
        yourBike.speedUp(10);
        yourBike.setGear(2);
        yourBike.setCadence(40);
        yourBike.speedUp(10);
        yourBike.printDescription();

    }
}
