package week2.t2;


public class Bicycle implements Cloneable {
    // the Bicycle class has
    // three fields
    private int cadence;
    private int gear;
    private int speed;
    // add an instance variable for the object ID
    private int id;
    // add a class variable for the
    // number of Bicycle objects instantiated
    private static int numberOfBicycles = 0;

    // the Bicycle class has
    // one constructor
    public Bicycle(int startCadence, int startSpeed, int startGear) {
        gear = startGear;
        cadence = startCadence;
        speed = startSpeed;
        // increment number of Bicycles
        // and assign ID number
        id = ++ numberOfBicycles;

    }


    void setCadence(int newValue) {
        cadence = newValue;
    }

    void setGear(int newValue) {
        gear = newValue;
    }

    void speedUp(int increment) {
        speed += increment;
    }

    public void printDescription() {
        System.out.println("The " + this.id + " Bike " +
                "\nBike is " + "in gear " + this.gear
                + " with a cadence of " + this.cadence +
                " and travelling at a speed of " + this.speed + ". ");
        System.out.println("cadence:" +
                cadence + " speed:" +
                speed + " gear:" + gear);
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "week2.t2.Bicycle{" +
                "cadence=" + cadence +
                ", gear=" + gear +
                ", speed=" + speed +
                ", id=" + id +

                '}';


    }
}
