package week4.t4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CompareTextFiles {
    public static void main(String[] args) throws IOException {
        BufferedReader text1 = new BufferedReader(new FileReader("src/main/java/week4/t4/text2.txt"));
        BufferedReader text2 = new BufferedReader(new FileReader("src/main/java/week4/t4/text2.txt"));
        String line1 = text1.readLine();
        String line2 = text2.readLine();
        boolean areEqual = true;

        while (line1 != null || line2 != null) {
            if (line1 == null || line2 == null) {
                areEqual = false;

                break;
            } else if (!line1.equalsIgnoreCase(line2)) {
                areEqual = false;

                break;
            }
            line1 = text1.readLine();
            line2 = text2.readLine();
        }

        if (areEqual) {
            System.out.println("YES");
        } else {
            System.out.println("No");


        }

    }

}