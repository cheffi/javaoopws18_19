package week4.t6;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: I've Been Everywhere, Man
 * Link: https://open.kattis.com/contests/pp5rtp/problems/everywhere;
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 11/08/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.10s
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Everywhere {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int t = scanner.nextInt();
        ArrayList<String> list = new ArrayList<>();
        String m;
        int count;

        for (int i = 0; i < t; i++) {

            list.clear();
            count = 0;
            int w = scanner.nextInt();

            for (int j = 0; j < w; j++) {

                m = scanner.next();

                if (!list.contains(m)) {
                    list.add(m);
                    count++;
                }
            }
            System.out.println(count);
        }
    }
}