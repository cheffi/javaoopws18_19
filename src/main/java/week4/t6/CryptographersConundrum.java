package week4.t6;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Cryptographer's Conundrum
 * Link: https://open.kattis.com/contests/pp5rtp/problems/conundrum;
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 11/08/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.07s
 */

import java.util.Scanner;

public class CryptographersConundrum {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String s = scanner.nextLine();
        char array[] = new char[3];
        array[0] = 'P';
        array[1] = 'E';
        array[2] = 'R';
        int count = 0;
        int j = 0;

        for (int i = 0; i < s.length(); i++) {

            if (j == 3) {
                j = 0;
            }
            if (array[j] != s.charAt(i)) {
                count++;
            }
            j++;
        }
        System.out.println(count);
    }
}
