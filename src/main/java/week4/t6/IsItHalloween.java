package week4.t6;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: IsItHalloween.com
 * Link: https://open.kattis.com/problems/isithalloween
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 8/11/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.07 s
 */

public class IsItHalloween {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Scanner input = new Scanner(System.in);
        String date = input.nextLine();
        if (date.equals("OCT 31") || date.equals("DEC 25")) {
            System.out.print("yup");
        } else {
            System.out.print("nope");
        }

    }

}
