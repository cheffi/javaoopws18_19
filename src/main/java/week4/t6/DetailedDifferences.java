package week4.t6;

import java.util.Scanner;

/**
 * Advanced Object Oriented Programming with Java, WS 2018
 * Problem: Detailed Differences
 * Link: https://open.kattis.com/problems/detaileddifferences
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 8/11/2018
 * Method: Ad-Hoc
 * Status: Accepted
 * Runtime: 0.19 s
 */

public class DetailedDifferences {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        for (int i = 0; i < n; i++) {
            String string1 = input.next();
            String string2 = input.next();
            System.out.print(string1 + "\n" + string2 + "\n");
            for (int j = 0; j < string1.length(); j++) {
                //checks if the char at the same position of the two strings are the same
                if (string1.charAt(j) == string2.charAt(j)) {
                    System.out.print(".");
                } else {
                    System.out.print("*");
                }
            }
            System.out.println();
        }

    }

}
