package week5.t1;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Exam Redistribution
 * Link: https://open.kattis.com/problems/redistribution
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime: 0.07s
 */


import java.util.Scanner;


public class Redistribution {
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        Integer[][] rooms2 = new Integer[n][2];


        for (int i = 0; i < n; i++) {
            rooms2[i][0] = i + 1;
            rooms2[i][1] = input.nextInt();
        }

        int temp;
        for (int i = 0; i < n - 1; ++i) {
            if (rooms2[i][1] < rooms2[i + 1][1]) {
                temp = rooms2[i][1];
                rooms2[i][1] = rooms2[i + 1][1];
                rooms2[i + 1][1] = temp;

                temp = rooms2[i][0];
                rooms2[i][0] = rooms2[i + 1][0];
                rooms2[i + 1][0] = temp;
            }
        }

        int total = 0;
        for (int i = 1; i < n; i++) {
            total += rooms2[i][1];
        }

        if (rooms2[0][1] > total) {
            System.out.println("impossible");
        } else {
            for (int i = 0; i < n; i++) {
                if (i == n - 1) {
                    System.out.print(rooms2[i][0]);
                } else
                    System.out.print(rooms2[i][0] + " ");
            }

        }
    }
}