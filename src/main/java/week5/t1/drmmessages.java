package week5.t1;

import java.util.Scanner;

public class drmmessages {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String firstString, secondString, resultString = "";
        String alphabetValue = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String input = scanner.nextLine().toUpperCase();
        int firstRotValue = 0;
        int secondRotValue = 0;
        firstString = input.substring(0, input.length() / 2);
        secondString = input.substring(firstString.length());

        StringBuilder firstBuilder = new StringBuilder(firstString);
        StringBuilder secondBuilder = new StringBuilder(secondString);
        StringBuilder resultBuilder = new StringBuilder(resultString);
        for (int i = 0; i < firstString.length(); i++) {
            firstRotValue += alphabetValue.indexOf(firstString.charAt(i));
            secondRotValue += alphabetValue.indexOf(secondString.charAt(i));
        }
        for (int i = 0; i < firstString.length(); i++) {
            int letterRotValue = firstRotValue + alphabetValue.indexOf(firstString.charAt(i));
            int letterRotValue2 = secondRotValue + alphabetValue.indexOf(secondString.charAt(i));
            while (letterRotValue >= 26) {
                letterRotValue -= 26;
            }
            firstBuilder.setCharAt(i, alphabetValue.charAt(letterRotValue));
            while (letterRotValue2 >= 26) {
                letterRotValue2 -= 26;
            }
            secondBuilder.setCharAt(i, alphabetValue.charAt(letterRotValue2));
            int resultRotValue = alphabetValue.indexOf(firstBuilder.charAt(i)) + alphabetValue.indexOf(secondBuilder.charAt(i));

            if (resultRotValue <= 25) {
                resultBuilder.append(alphabetValue.charAt(resultRotValue));
            } else {
                resultRotValue -= 26;
                resultBuilder.append(alphabetValue.charAt(resultRotValue));

            }
        }
        System.out.println(resultBuilder);

    }

}
