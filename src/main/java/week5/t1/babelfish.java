package week5.t1;


import java.util.*;

public class babelfish {
    
    public static void main(String[] args) {
        HashMap<String,String> dictionary = new HashMap<>();
  
        
        
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        while (input.contains(" ")){
           
            String key  = input.split(" ")[1];
            String value  = input.split(" ")[0];

                dictionary.put(key,value);
                input = scanner.nextLine();
            
        }

        while (scanner.hasNextLine()) {
            String res = scanner.nextLine();
            System.out.println(dictionary.getOrDefault(res, "eh"));
        }
    }
}
                 