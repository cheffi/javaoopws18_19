package week5.t1;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Palindrome Names
 * Link: https://open.kattis.com/problems/names
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 28 11 2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime: 0.07s
 */


import java.util.*;


public class names {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        StringBuilder name = new StringBuilder(sc.nextLine());
        int maxl = name.length() / 2 + 1;
        int max = maxl;
        for (int i = 0; i < maxl; i++) {
            int temp = 0;
            for (int j = 0; j < name.length() / 2; j++) {
                if (j < name.length() - j - 1 && (name.charAt(j) != name.charAt(name.length() - j - 1))) {
                    temp++;
                }
            }
            if (temp < max) {

                max = temp;
            }
            name.append(" ");
        }
        System.out.println(max);
    }
}
