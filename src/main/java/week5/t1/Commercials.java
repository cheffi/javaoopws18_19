package week5.t1;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Radio Commercials
 * Link: https://open.kattis.com/contests/eu6hf6/problems/commercials;
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 11/15/2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime: 0.42s
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Commercials {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> list = new ArrayList<>();

        int n = scanner.nextInt(); //number of breaks
        int p = scanner.nextInt(); //price for a break

        for (int i = 0; i < n; i++) {
            list.add(scanner.nextInt() - p);
        }
        scanner.close();

        int extraProfit = list.get(0);
        int sequence = list.get(0);

        for (int i = 1; i < list.size(); i++) {
            if (sequence < 0) {
                sequence = list.get(i);
            } else {
                sequence += list.get(i);
            }
            if (sequence >= extraProfit) {
                extraProfit = sequence;
            }
        }
        System.out.println(extraProfit);
    }
}
