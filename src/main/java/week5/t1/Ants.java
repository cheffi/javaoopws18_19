package week5.t1;

/**
 * Advanced Object Oriented Programming with Java, WS 2018/19
 * Problem: Ants
 * Link: https://open.kattis.com/contests/eu6hf6/problems/ants;
 *
 * @author Kevin Kopp
 * @author Anas Mesbah
 * @author Jannik Kluge
 * @version 1.0, 11/19/2018
 * Method : Ad-Hoc
 * Status: Accepted
 * Runtime: 0.83s
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Ants {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> list = new ArrayList<>();
        ArrayList<Integer> latest = new ArrayList<>();

        int t = scanner.nextInt();
        String result[] = new String[t];


        for (int i = 0; i < t; i++) {

            latest.clear();
            list.clear();
            int earliest = 0;

            int l = scanner.nextInt(); //the length l of the pole (in cm)
            int n = scanner.nextInt(); //number of ants


            for (int j = 0; j < n; j++) {
                list.add(scanner.nextInt());
                earliest = Math.max(Math.min(list.get(j), l - list.get(j)), earliest);

            }
            Collections.sort(list);

            latest.add(list.get(0));
            latest.add(list.get(n - 1));
            latest.add(l - list.get(0));
            latest.add(l - list.get(n - 1));

            Collections.sort(latest);
            result[i] = earliest + " " + latest.get(3);
        }
        for (int i = 0; i < t; i++) {
            System.out.println(result[i]);
        }
    }
}
