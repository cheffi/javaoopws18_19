package week5.t1;

import java.util.Scanner;

public class Zamka {
    static int l, d, x, n, m;

    public static int berechneQuersumme(int zahl) {
        int summe = 0;
        while (0 != zahl) {
            // addiere die letzte ziffer der uebergebenen zahl zur summe
            summe = summe + (zahl % 10);
            // entferne die letzte ziffer der uebergebenen zahl
            zahl = zahl / 10;
        }
        return summe;
    }

    public static int min(int l, int d, int x) {

        int sum = 0;

        for (int i = l; i <= d; ++i) {
            sum = berechneQuersumme(i);
            if (sum == x)
                return i;
            break;

        }
        return sum;
    }


    public static int max(int l, int d, int x) {

        int sum = 0;

        for (int i = d; i >= l; --i) {
            sum = berechneQuersumme(i);
            if (sum == x)
                return i;
            break;
        }
        return sum;
    }


    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);

        l = scanner.nextInt();
        d = scanner.nextInt();
        x = scanner.nextInt();
        n = min(l, d, x);
        m = max(l, d, x);

        System.out.println(n);
        System.out.println(m);


    }
}
