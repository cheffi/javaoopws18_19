import java.util.HashSet;
import java.util.Scanner;


public class CD {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n, m;

        n = scanner.nextInt();
        m = scanner.nextInt();
        while (n != 0 && m != 0) {
            int count = 0;
            HashSet<Integer> jack = new HashSet<>(n);
            HashSet<Integer> jill = new HashSet<>(m);
            for (int i = 0; i < n; i++) {
                int cdOfJack = scanner.nextInt();
                jack.add(cdOfJack);
            }
            for (int i = 0; i < m; i++) {
                int cdOfJill = scanner.nextInt();
                jill.add(cdOfJill);
            }
            if (n >= m) {
                for (int i : jill)
                    if (jack.contains(i))
                        count++;
            } else{
                for (int i : jack)
                    if (jill.contains(i))
                        count++;
            }
            
             n = scanner.nextInt();
             m = scanner.nextInt();
            System.out.println(count);
        }


    }
}
