package utils;

import java.awt.*;

public class Constants {
    public static final int FPS = 30;

    public static final int WINDOW_WIDTH = 500;
    public static final int WINDOW_HEIGHT = 500;
    public static final int PLAYING_AREA_WIDTH = 700;
    public static final int PLAYING_AREA_HEIGHT = 500;

    public static final int SCALE_UNIT = PLAYING_AREA_WIDTH / 20;

    public static final int WINDOW_X = 0;
    public static final int WINDOW_Y = 0;

    public static final boolean WINDOW_RESIZEABLE = false;

    public static final String WINDOW_TITLE_PONG = "Pong";
    public static final Color MAIN_MENU_BACKGROUND_COLOR = Color.red;
    public static final Color LEVEL_1_DOTTED_LINE_COLOR = new Color(255, 255, 255, 100);
    public static final Color LEVEL_1_BACKGROUND_COLOR = Color.black;

    public static final int PLAYER_WIDTH = 15;
    public static final int PLAYER_HEIGHT = 100;
    public static final int PLAYER_1_SPAWN_POSITION_X = 20;
    public static final int PLAYER_1_SPAWN_POSITION_Y = PLAYING_AREA_HEIGHT / 2 - PLAYER_HEIGHT / 2;
    public static final float MAXSPEED = 200 * SCALE_UNIT;
    public static final float ACCELERATION_STRENTGH = 20f * SCALE_UNIT;
    public static final float PADDLE_DRAG = 0.01f; // 0: no drag, 1: paddle instantly stops

    public static final int PLAYER_2_SPAWN_POSITION_X = PLAYING_AREA_WIDTH - PLAYER_WIDTH - 20;
    public static final int PLAYER_2_SPAWN_POSITION_Y = PLAYING_AREA_HEIGHT / 2 - PLAYER_HEIGHT / 2;

    public static final int BALL_WIDTH = 20;
    public static final int BALL_HEIGHT = 20;

    public static final int BALL_SPAWN_POSITION_X = PLAYING_AREA_WIDTH / 2;
    public static final int BALL_SPAWN_POSITION_Y = PLAYING_AREA_HEIGHT / 2;

    public static final Color BALL_COLOR = Color.yellow;

    public static final Color PLAYER_1_COLOR = Color.cyan;
    public static final Color PLAYER_2_COLOR = Color.magenta;

    // Console colors
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
}
