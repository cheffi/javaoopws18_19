package pong.agents;

import pong.Game;
import pong.objects.Paddle;

public abstract class Agent {
    final Game game;
    public final String name;
    private Paddle paddle;

    public Agent(String name, Game game) {
        this.name = name;
        this.game = game;
    }

    public void assign(Paddle p){
        paddle = p;
    }

    void accUp() {
        paddle().accelerate(-1);
    }

    void accDown() {
        paddle().accelerate(1);
    }

    public abstract void update();

    public Paddle paddle() {
        return paddle;
    }
}
