package pong.agents;

import pong.Game;

public class HarderAI extends Agent {
    public HarderAI(String name, Game game) {
        super(name, game);
    }

    @Override
    public void update() {
        if (game.model.getAllBalls().getFirst().y() > paddle().y())
            accDown();
        else
            accUp();
    }
}
