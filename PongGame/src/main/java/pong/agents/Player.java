package pong.agents;

import pong.Game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Player extends Agent implements KeyListener {
    private int inputCounter = 0;

    public Player(String name, Game game) {
        super(name, game);
        game.view.registerKeyListener(this);
    }

    @Override
    public void update() {
        // we don't need to do anything here. Controls are already handled in keyPressed(KeyEvent e)
    }

    /*
    ////////// KEY EVENTS /////////
     */

    /**
     * Whereas the AI is usually using the update method to control the paddle,
     * a Player controls it based on key events.
     * AI must control the paddle without user Interaction, so it uses the update Method.
     * @param e
     */
    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        inputCounter++;
        switch(key){
            case KeyEvent.VK_UP:
                accUp();
                break;
            case KeyEvent.VK_DOWN:
                accDown();
                break;
            case KeyEvent.VK_Q:
                game.stop();
                break;
                default: inputCounter --;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }//class

    @Override
    public String toString() {
        return name + " inputs received: " + inputCounter;
    }
}
