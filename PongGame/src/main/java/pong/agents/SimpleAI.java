package pong.agents;

import pong.Game;

public class SimpleAI extends Agent {

    public SimpleAI(String name, Game game) {
        super(name, game);
    }

    @Override
    public void update() {
        if (game.model.getAllBalls().getFirst().y() > paddle().y())
            accDown();
        else
            accUp();
    }
}
