package pong;

import javax.swing.*;
import java.awt.*;

public class PongPanel extends JPanel {
    // static int netPos = 345;
    final GameView view;

    public PongPanel(GameView v){
        view = v;
    }
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        view.render(g);
        g.dispose();
    }
}