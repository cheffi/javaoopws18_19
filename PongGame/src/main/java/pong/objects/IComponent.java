package pong.objects;

import pong.Game;

public abstract class IComponent {
    public static Game game; // Warning: does not work for multiple games at the same time
    IComponent(){
        game = Game.get();
    }

    public abstract void update();

    public abstract void draw();

    public abstract void reset();
}
