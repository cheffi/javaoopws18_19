package pong.objects;

import pong.GameView;
import utils.Vector2;

import java.util.List;

import static pong.GameView.log;
import static utils.Constants.*;

public class Ball extends RigidBody {
    public Ball(GameView view) {
        super(view.makeBallShapeAtCenter());
        log("ball created", this);
    }

    /**
     * when this method is invoked, all other moving objects have already be updated.
     */
    @Override
    public void update() {
        super.update();
        // check collisions with paddles
        List<Paddle> paddles = game.model.getAllPaddles();
        for (Paddle paddle : paddles) {
            if (this.intersects(paddle)){
                // collision with paddle

                paddle.collidesWithBall(this); //TODO handle collision in Ball class or use RigidBody

            }
        }
       reset();

    }

    public enum Speed {
        SLOW,
        MEDIUM,
        FAST
    }

    public void setSpeed(Speed category){
        if (speed().x == 0 & speed().y == 0)
            speed(new Vector2().randomize());
        switch (category){
            case SLOW:   currentSpeed = currentSpeed.scaledToMagnitude(2 * SCALE_UNIT); break;
            case MEDIUM: currentSpeed = currentSpeed.scaledToMagnitude(3 * SCALE_UNIT); break;
            case FAST:   currentSpeed = currentSpeed.scaledToMagnitude(5 * SCALE_UNIT); break;
        }
    }

    @Override
    public void draw() {
        game.view.drawBall(this);
    }

    @Override
    public void reset() {

        if(location().x < 0 || location().x > PLAYING_AREA_WIDTH){
            setX(BALL_SPAWN_POSITION_X);
            setY(BALL_SPAWN_POSITION_Y);

        }

    }
}
