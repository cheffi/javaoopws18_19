package pong.objects;

import utils.Vector2;

import java.awt.*;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;

import static pong.GameView.log;

/**
 * An Object that can be moved and accelerated, in 2d space.
 * Can be moved more precisely than pixel wise.
 * Can collide with other RigidBody Objects.
 */
public abstract class RigidBody extends IComponent {
    private Vector2 loc;
    Vector2 currentSpeed; // meassured in ScaleUnits per Second
    Rectangle2D shape;

    public RigidBody(Rectangle2D hitbox) {
        this.shape = hitbox;
        loc = new Vector2(hitbox.getBounds2D().getX(), hitbox.getBounds2D().getY());
        currentSpeed = new Vector2(0,0);
    }

    /**
     * Move Object according to speed.
     */
    @Override
    public void update(){
        //TODO find a proper speed setting
        Vector2 step = currentSpeed.scale(game.dTime());
        move(step);

        // Collision with walls
        boolean isTooLow = bottom() > game.model.playArea().y;
        boolean isTooHigh = top() < 0;
        if (isTooLow || isTooHigh){
            currentSpeed.y = - currentSpeed.y;
            if(isTooLow) loc.y = (game.model.playArea().y) - height();
            else if(isTooHigh) setY(0);
        }
    }
    private void move(Vector2 v) {
        loc = loc.plus(v);
    }

    public void accelerate(Vector2 directionStrength) {
        currentSpeed = currentSpeed.plus(directionStrength);
    }

    /**
     * This Method should be Overridden by child classes.
     * It is only implemented to give an easy starting point for debugging what
     * shapes have not been drawn on their own.
     */
    @Override
    public void draw() {
        game.view.graphics().setColor(Color.red);
        game.view.graphics().translate(loc.x, loc.y);
        game.view.graphics().draw(shape);
        game.view.graphics().translate(-loc.x, -loc.y);
    }

    void setX(double x) {
        shape.getBounds().x = (int) x;
        loc.x = x;
    }

    void setY(double y) {
        shape.getBounds().y = (int) y;
        loc.y = y;
    }

    public boolean intersects(RigidBody other){
        Shape otherShape = other.shape;
        if( this.shape.getBounds().intersects(otherShape.getBounds()) ){
            Area a = new Area(shape);
            log("Ball intersects with Object", this);
            a.intersect(new Area(otherShape));
            if(a.isEmpty())
                return false;
            return true;
        }
        return false;
    }

    protected Vector2 speed() {
        return currentSpeed;
    }

    protected void speed(Vector2 value) {
        currentSpeed = value;
    }
    public Shape shape() {
        return shape;
    }

    public Vector2 location() {
        return loc;
    }

    public double x() {
        return loc.x;
    }

    public double y() {
        return loc.y;
    }

    public double width() {
        return shape.getBounds2D().getWidth();
    }

    public double height() {
        return shape.getBounds2D().getHeight();
    }

    private double top() {
        return y();
    }

    private double bottom() {
        return y() + shape.getBounds2D().getHeight();
    }

    @Override
    public String toString() {
        return String.format("%s -- loc: %s\tcurrentSpeed: %s",
                this.getClass().getSimpleName(), loc, currentSpeed);
    }
}
