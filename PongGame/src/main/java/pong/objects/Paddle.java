package pong.objects;

import pong.Game;
import pong.agents.Agent;
import utils.Constants;
import utils.Vector2;

import static pong.GameView.log;
import static utils.Constants.ACCELERATION_STRENTGH;
import static utils.Constants.MAXSPEED;

public class Paddle extends RigidBody {
    private final Game game;
    private final Agent owner;

    public Paddle(Game game, Agent user, int x) {
        super(game.view.makePaddleShape(x));
        this.game = game;
        this.owner = user;
        log("Paddle created", this);
    }

    /**
     * Cannot move along the x coordinate
     * @param strength -1: full power up | 1: full power down.
     */
    public void accelerate(double strength) {
        assert (Math.abs(strength) <=1);

        double deltaSpeed = strength * ACCELERATION_STRENTGH * game.dTime();
        accelerate(new Vector2(0, deltaSpeed));

        // limit speed TODO extract in separate method
        double ySpeed = speed().y;
        if(ySpeed > MAXSPEED)
            speed().y = MAXSPEED;
        else if(ySpeed < -MAXSPEED)
            speed().y = -MAXSPEED;
    }

    @Override
    public void update() {
        super.update();
        speed(speed().scale(1 - Constants.PADDLE_DRAG)); // slowdown paddle

    }

    @Override
    public void reset() {

    }

    /*
    @Override
    public void draw(Graphics2D graphics2D) {
        graphics2D.setColor(Color.white);
        graphics2D.fillRect(shape.x, shape.y, shape.width, shape.height);
    }
    */

    public void collidesWithBall(Ball ball) {
        ball.accelerate(new Vector2( - ball.speed().x * 2, 0));
    }

    @Override
    public String toString() {
        return super.toString() + " " + owner.toString();
    }
}