package pong;

import pong.agents.Agent;
import pong.objects.IComponent;

public class GameController {
    private final Game game;

    public GameController(Game game) {
        this.game = game;
    }

    public GameController init() {
        return this;
    }
    /* Update Order:
     *  1. All Objects, that are immobile are being updated. (e.g goalLines / walls)
     *  2. All Objects that are controlled by an Agent. (e.g. paddles)
     *  3. All Objects that are purely reacting to the state of the game. (e.g. balls)
     * The Order ensures, that collisions between Physics objects (Ball) and Agent Objects (Paddle)

     */

    /**
     * There are three levels of update calls.
     * ur in the same order.
     */
    public void callUpdates() {
        // game.model.getAllStatics().forEach(IComponent::update); // walls and goals are not implemented as such yet.
        // first move all the paddles. When this is done, balls can be considered
        // as if they were placed in a static environment.
        game.model.getAllPaddles().forEach(IComponent::update);
        game.model.getAllBalls().forEach(IComponent::update);
        game.model.getAllAgents().forEach(Agent::update);
    }
}
