package pong;

import pong.objects.Ball;
import pong.objects.Paddle;
import utils.Constants;
import utils.Vector2;

import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;

import static utils.Constants.*;

public class GameView {
    private Graphics2D graphics;
    private Game game;
    private final PongFrame frame;
    private final PongPanel panel;

    public GameView(Game g){
        game = g;
        panel = new PongPanel(this);
        frame = new PongFrame(panel);
    }

    public GameView init() {
        return this;
    }

    public void render() {
        // panel.repaint(); // done in panel paint component
        panel.invalidate();
    }

    public void render(Graphics g) {
        graphics = (Graphics2D) g;

        drawNet(g);
        game.model.getAllBalls().forEach(this::drawBall);
        game.model.getAllPaddles().forEach(this::drawPaddle);
        // game.model.getComponents().forEach((c) -> c.draw());
        frame.repaint();
        //frame.revalidate();
    }

    ///////////// Tools /////////////////
    public Point getMiddle() {
        return new Point(PLAYING_AREA_WIDTH / 2, PLAYING_AREA_HEIGHT / 2);
    }
    public void registerKeyListener(KeyListener player) {
        frame.addKeyListener(player);
    }

    public PongFrame getFrame() {
        return frame;
    }

    ///////////// creating object "Sprites" /////////////////
    public Rectangle2D.Double makePaddleShape(int x) {
        return new Rectangle2D.Double(x, getMiddle().y, PLAYER_WIDTH, PLAYER_HEIGHT);
    }

    public Rectangle2D.Double makeBallShapeAtCenter() {
        Point middle = getMiddle();
        return new Rectangle2D.Double(middle.x, middle.y, BALL_WIDTH, BALL_HEIGHT);
    }

    //////////////// Notifications /////////////////
    public static void message(String message) {
        System.out.println(message);
    }
    public static void print(Object o){
        System.out.println(o.toString());
    }
    public static void log(Object o){
        System.out.println("[LOG] " + o.toString());
    }
    public static void log(String message, Object o) {
        log(message);
        System.out.println(ANSI_BLUE + o.toString() + ANSI_RESET);
    }

    public static void warning(Object o){
        System.out.println(Constants.ANSI_RED + o.toString() + Constants.ANSI_RESET);
    }
    public void drawBall(Ball ball) {
        graphics.setColor(BALL_COLOR);
        Vector2 loc = ball.location();
        //graphics.draw(ball.shape());
        graphics.fillOval((int)loc.x, (int)loc.y, (int)ball.width(), (int)ball.height());
    }

    public void drawPaddle(Paddle paddle) {
        graphics.setColor(Color.yellow);
        graphics.fillRect((int)paddle.x(), (int)paddle.y(), (int)paddle.width(),(int)paddle.height());
    }

    private void drawNet(Graphics g) {
        panel.setBackground(Color.black);
        g.setColor(Color.WHITE);
        for (int i = 0; i <50 ; i++) {
            g.fillRect(PLAYING_AREA_WIDTH/ 2, i*20,10 ,10);
        }
    }

    public Graphics2D graphics() {
        return graphics;
    }

    public void showScreen() {
        frame.setVisible(true);
    }
}
