package pong;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static pong.GameView.log;

/**
 * This class allows the user to print data for debugging during game play.
 * while the game is running press a key like 's' or 'd' to log current data.
 */
public class Monitor implements KeyListener {
    private final Game game;

    Monitor(Game game){
        this.game = game;
        game.view.registerKeyListener(this);
    }
    @Override
    public void keyTyped(KeyEvent keyEvent) {
        switch (keyEvent.getKeyChar()){
            case 's':
                game.model.getAllBalls().forEach(ball -> log("all ", ball));
                break;
            case 'd':
                game.model.getAllPaddles().forEach(paddle -> log("all ", paddle));
                break;
        }
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {

    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }
}
