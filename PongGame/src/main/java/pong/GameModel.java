package pong;


import pong.agents.Agent;
import pong.agents.Player;
import pong.agents.SimpleAI;
import pong.objects.Ball;
import pong.objects.IComponent;
import pong.objects.Paddle;
import utils.Vector2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static utils.Constants.*;

/**
 * Handles game data.
 */
public class GameModel implements ActionListener {
    private static final double FIELD_WIDTH = PLAYING_AREA_WIDTH;
    private static final double FIELD_HEIGHT = PLAYING_AREA_HEIGHT;
    private final Game game;

    static final int PADDLE_MARGIN = 15; // how far away is AI the paddle from the walls.
    private Paddle left;
    private Paddle right;
    private Ball ball;
    private List<IComponent> components = new ArrayList<>();
    private List<Paddle> paddles        = new ArrayList<>();
    // private List<Agent> agents          = new ArrayList<>();

    int scoreLeft = 0, scoreRight = 0;
    private Agent leftAgent;
    private Agent rightAgent;

    public GameModel(Game g) {
        this.game = g;
    }

    public GameModel init() {
        // Spieler erstellen
        Agent player1 = new Player("ich", game);
        Agent player2 = new SimpleAI("bot", game);
        //agents.add(player1);
        //agents.add(player2);

        leftAgent = player1;
        rightAgent = player2;
        // Spielfeld aufbauen

        // TODO would be nicer if there was something like a PlayArea or PlayField class
        left = createPaddle(player1, PADDLE_MARGIN);
        right = createPaddle(player2,  game.view.getFrame().getWidth() - PADDLE_MARGIN*2 );
        createBall();
        return this;
    }

    public enum Side{
        left,right
    }
    public void replace(Side side, Class< ? extends Agent> agentTypeClass, String name){
        switch (side){
            case left:
            case right:
        }
    }

    public void createPlayer(String name){
        Agent player = new Player(name, game);
    }
    public void createSimpleAI(String name){
        Agent player = new SimpleAI(name, game);
    }

    private void createBall() {
        ball = new Ball(game.view);
        ball.setSpeed(Ball.Speed.MEDIUM);
        components.add(ball);
    }

    private Paddle createPaddle(Agent user, int xPos) {
        Paddle paddle = new Paddle(game, user, xPos);
        components.add(paddle);
        paddles.add(paddle);
        user.assign(paddle);
        return paddle;
    }

    /**
     * there is only one ball here.
     */
    public LinkedList<Ball> getAllBalls() {
        LinkedList<Ball> result = new LinkedList<Ball>();
        result.add(ball);
        return result;
    }

    public List<Paddle> getAllPaddles() {
        return paddles;
    }

    public List<IComponent> getComponents(){
        return components;
    }

    public void start(){
        int delay = 1000 / FPS;
        Timer timer = new Timer(delay, this);
        timer.start();
    }

    @Override @Deprecated
    public void actionPerformed(ActionEvent e) {
        // System.out.println("action performed");
        game.controller.callUpdates();
        // game.controller.handleCollisions();
    }

    public List<Agent> getAllAgents() {
        List <Agent> list = new LinkedList<>();
        list.add(leftAgent);
        list.add(rightAgent);
        return list;
    }


    public Vector2 playArea() {
        return new Vector2(FIELD_WIDTH, FIELD_HEIGHT);
    }
}
