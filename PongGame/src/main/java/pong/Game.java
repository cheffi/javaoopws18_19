package pong;

import java.util.concurrent.TimeUnit;

import static pong.GameView.message;
import static pong.GameView.warning;
import static utils.Constants.FPS;

/**
 * Responsible for Running a game of Pong
 */
public class Game {
    private static Game currentlyInitializingInstance;
    // Model-View-Controller Pattern
    // Model-View-Controller Pattern
    final public GameController controller;
    final public GameView view;
    final public GameModel model;
    private final Monitor monitor;
    private boolean running = false;
    private MyTimer timer;


    public Game(){
        currentlyInitializingInstance = this; // used so Instance does not need to be passed down when initializing.
        controller = new GameController(this);
        model = new GameModel(this);
        view = new GameView(this);
        controller.init();
        model.init();
        view.init();
        monitor = new Monitor(this);
        timer = new MyTimer();
    }


    public static Game get() {
        return currentlyInitializingInstance;
    }
    // TODO A class Physics would be nice to have
    // final Physics = new Physics?


    public void run() {
        running = true;
        view.showScreen();
        while(running){
            timer.waitIfNeeded();
            controller.callUpdates();
            view.render();
            // view.render();  // update handled in drawComponent()
        }
        message("Game Over");
    }

    public void stop(){
        running = false;
    }

    public static void main(String[] args) {

        Game game1 = new Game();
        game1.run();
    }

    /**
     * Get time between this step and previous.
     * @return time in seconds
     */
    public float dTime(){
        //return 0.1f; // for debugging. So you can step through the code and simulate 10 fps
        return timer.dt / 1000; //
    }

    /**
     * For more Transparancy here is a custom Timer class
     */
    class MyTimer{

        // all times in seconds
        private long lastTime = System.currentTimeMillis();
        private float dt = 0;

        int frameCounter = 0;

        /**
         * This method returns when enough time has passed since last call. It will block the current threat until then.
         * Uses the constant FPS for the time interval.
         */
        private void waitIfNeeded() {
            try {
                long current = System.currentTimeMillis();
                frameCounter++;
                dt = current - lastTime;
                double waitTime = 1000d / FPS;
                if (dt < waitTime) {
                    // wait until it is time for the new frame
                    long remainingTimeInMillis = (long) ((waitTime - dt));
                    TimeUnit.MILLISECONDS.sleep(remainingTimeInMillis);
                }
                lastTime = current;
            } catch (InterruptedException e) {
                warning("Wait interrupted");
            }
        }
    }
}
