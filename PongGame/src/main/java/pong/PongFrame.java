package pong;

import javax.swing.*;
import java.awt.*;

import static utils.Constants.*;


public class PongFrame extends JFrame {



    public PongFrame(PongPanel gamePanel) {
        super();
        getContentPane().setPreferredSize(new Dimension(PLAYING_AREA_WIDTH, PLAYING_AREA_HEIGHT));
        setResizable(false);
        setTitle(WINDOW_TITLE_PONG);
        add(gamePanel);
        setFocusable(true);
        requestFocus();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(0, 0, PLAYING_AREA_WIDTH, PLAYING_AREA_HEIGHT);
        pack();
    }

    public void redraw(){
        getBufferStrategy().show();
    }
}
