package pong;

import javax.swing.*;
import java.awt.*;

import static utils.Constants.*;

public class MenuFrame extends JFrame {


    public MenuFrame() {

        super();
        getContentPane().setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setResizable(false);
        setTitle("Menu");
        setFocusable(true);
        requestFocus();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        pack();
    }
}
